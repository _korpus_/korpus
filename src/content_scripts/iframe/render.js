"use strict";

if (window != top && window.name == "k_panel") {

    var channel,
        korpus, data, lang, dBody, self, nd = 10;

    var fragment = document.createDocumentFragment();

    function _() {
        var self = this;
        chrome.storage.local.get("dictionaries", d => self.dictionaries = d.dictionaries);
        this.onDOMContentLoaded = function() {
            channel = new MessageChannel();
            window.parent.postMessage({
                    cmd: "DOM__content__loaded"
                },
                "*", [channel.port2]
            );
            channel.port1.onmessage = function(e){
                data = e.data;
                if (data.dictionary) {
                    if (window.location.origin != (a => a[0] + "//" + a[2])(data.url.split("/"))) return;
                    lang = data.lang;
                    self._render();
                }
            };
        };
    }

    _.prototype = {
        _sanitize(){
            if (!/googletranslate|rae/.test(data.dictionary)) {
                window.stop();
                document.head.textContent = "";
            }
            dBody = document.body;
            dBody.style = "";
            dBody.className = "";
            dBody.style.backgroundImage = "unset";
            document.querySelectorAll("script").forEach(s=>s.remove());
        },
        _render(){
            self = this;
            this._sanitize();
            var container,
                section = document.createElement("section"),
                header = document.createElement("header"),
                footer = document.createElement("footer"),
                srch = this._appendSearch(),
                nav = this._appendNav();

            header.className = "k-header";
            header.appendChild(srch);
            header.appendChild(nav);
            section.className = "k-section";
            section.classList.add(data.dictionary);
            footer.className = "k-footer";

            if (/googletranslate/.test(data.dictionary) || /deepl/i.test(lang)) {
                this._appendInline(header, nav);
                srch.style.width = "33%"
                srch.style.opacity = 0;
            }

            this._cleanFragment(data);
            section.appendChild(fragment);
            footer.appendChild(this._copyright(data));

            container = this._appendContainer(dBody, container, data.dictionary);
            container.appendChild(header);
            container.appendChild(section);
            container.appendChild(footer);

            let channel = new MessageChannel();
            window.parent.postMessage({ cmd: "DOM__content__cleaned"}, "*", [channel.port2]);

            if (data.inlineTranslate) {
                if (data.console || data.inlineTranslate === "all") return;
                this._inlineTranslate(data);
            }
            //document.body.querySelectorAll("script").forEach(s=>s.remove());

        },
        _cleanFragment(data) {
            document.head.insertAdjacentHTML(
                "beforeend",
                '<meta charset="utf-8"> <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes, shrink-to-fit=yes"/>'
            );
         
            try {
                this._slices[data.dictionary]();
            } catch (e) {
                fragment.appendChild(document.createElement("br"));
                fragment.appendChild(document.createTextNode("..."));
            }
        },
        _slices : {
                babla: function() {
                    var content = dBody.querySelector(".content-column"),
                        audio = new Audio();
                    fragment.appendChild(content.querySelectorAll(".content")[0]);
                    for (let id of ["#translationsdetails1", "#synonyms", "#practicalexamples"]) {
                        let c = content.querySelector(id);
                        if (c) fragment.appendChild(content.querySelector(id));
                    }
                    for (let sound of [".sound", ".sound-inline"]) {
                        [].slice.call(fragment.querySelectorAll(sound)).map(a => {
                            a.onclick = function() {
                                var s = this.getAttribute("href");
                                audio.src =
                                    "https://en.bab.la/sound/" +
                                    s
                                    .slice(s.indexOf("(") + 1, s.indexOf(")"))
                                    .replace(/'/g, "")
                                    .split(",")
                                    .slice(0, 2)
                                    .join("/") +
                                    ".mp3";
                                audio.play();
                                return false;
                            };
                        });
                    }
                    return;
                },
                cnrtl: function() {
                    if (lang == "crisco") {
                        //this._addFontFamily("Alegreya");
                        self._slice(false, dBody, "#synonymes", fragment);
                        var a = fragment.querySelector("h3");
                        if (a) {
                            for (var i = 1; i < a.length; i++) {
                                a[i].parentNode.removeChild(a[i]);
                            };
                        }
                    } else if (lang == "bob") {
                        if (dBody.querySelector(".wikisearch")) self._slice(true, dBody, ".fpltemplate", fragment);
                        else self._slice(true, dBody, ".fpltemplate", fragment);
                    } else if (lang == "gaffiot" || lang == "logeion") {
                        var entries = dBody.textContent,
                            parser = new DOMParser(),
                            article = document.createElement("article"),
                            part = function(tag, txt, child_nodes = false, href) {
                                let h = document.createElement(tag);
                                if (!child_nodes) {
                                    h.textContent = txt;
                                    if (h.tagName == "A") h.href = '/' + entry.id;
                                } else {
                                    child_nodes = parser.parseFromString(child_nodes, 'text/html').querySelector("body").childNodes;
                                    child_nodes.forEach((elmt, index, obj) => h.appendChild(elmt));
                                }
                                article.appendChild(h);
                            };
                        article.className = "logeion"; 
                        if( lang == 'gaffiot') { 
                            entries = (window.location.href.indexOf('search') > 0) ? JSON.parse(entries) : dBody.querySelector('.detail');
                            if(Array.isArray(entries) && entries.length > 0) {
                                    var txt = '';                                    
                                    for (var entry of entries) {
                                        txt = (entry.raw + ' ' + entry.latin).replace(/[0-9]\s/g, '');
                                        part("a", txt, false, entry.id);
                                        part("br", "");
                                    }
                            } else {
                                entries = dBody.querySelector('.detail');
                                article.appendChild(entries);
                            }
                            data.url = `https://gaffiot.org`;
                        } else {
                            entries = JSON.parse(entries).detail;
                            if (entries.dicos.length > 0) {
                                for (var dico of entries.dicos) {
                                    if (["Gaffiot 2016", "DuCange"].includes(dico.dname)) {
                                        part("h3", dico.dname, false,'');
                                        part("p", dico.dname, dico.es[0], false, '')
                                    }
                                }
                            }
                            data.url = `http://logeion.uchicago.edu/${data.phrase}`;
                        }
                        fragment.appendChild(article);
                    } else {
                        let ul, a,
                            nav = function(arr) {
                                var ul = document.createElement("ul"),
                                    dir = arr.includes("Etymologie") ? "" : "definition/",
                                    path, d, a, li;
                                ul.className = "tab_box";
                                for (d of arr) {
                                    a = document.createElement("a");
                                    li = document.createElement("li");
                                    a.textContent = d;
                                    d = d.toLowerCase().replace("é", "e");
                                    path = (d == "dmf" ? "utilities/ADMF?query=" : (dir + d + "/"));
                                    a.href = "https://www.cnrtl.fr/" + path + data.phrase;
                                    li.appendChild(a);
                                    ul.appendChild(li);
                                }
                                return ul;
                            };

                        for (let dict of [
                                ["Académie9", "DMF", "Académie8", "BHVF", "Académie4"],
                                ["Définition", "Etymologie", "Synonymie", "Antonymie"]
                            ])
                            fragment.appendChild(nav(dict));

                        a = dBody.querySelector("#vtoolbar");
                        if (a) {
                            fragment.appendChild(a);
                            self._slice(false, dBody, "#contentbox", fragment)
                        } else {
                            a = dBody.querySelector("table");
                            if (/xxx;etym/.test(window.location.href)) a.querySelector("table").remove();
                            else
                                for (let i of [0, 1]) a.deleteRow(0);
                            fragment.appendChild(a);
                            ul = fragment.querySelector("table").querySelectorAll(".tab_box");
                            ul.forEach((td, i, o) => {
                                a = td.children[0];
                                a.href = a.href.replace(/definition\/dmf\//, "utilities/ADMF?query=").replace("?idf", "&idf");
                            });
                        }
                        for (a of [".syno_format", ".anto_format"]) {
                            a = fragment.querySelectorAll(a);
                            let str, f = function() {
                                data.phrase = this.href
                                    .match(/\/antonymie|synonymie\/\w*/)[0]
                                    .replace(/\/antonymie|synonymie\//, "");
                            };
                            if (a.length > 0) fragment.querySelectorAll(a).forEach((td, i, o) => {
                                a = td.children[0];
                                if (a.href.match(/\/antonymie|synonymie\/\w*/)[0])
                                    a.onclick = f;
                            });
                        }
                    }
                    return;
                },
                littre: function() {
                    self._slice(false, dBody, "section.definition", fragment);
                    return;
                },
                larousse: function() {
                    self._slice(true, dBody, ".col-md-8", fragment);
                    self._slice(true, dBody, ".col-md-8", fragment);
                    let audio = new Audio(),
                        play = function() {
                            audio.src = this.getAttribute("href");
                            audio.play();
                            return false;
                        }, snd;
                    [".linkaudio", ".lienson", ".lienson2", ".lienson3"].forEach(snd => {
                        snd = Array.from(fragment.querySelectorAll(snd));
                        if (snd.length > 0)
                            snd.forEach(a => {
                                a.classList.add("k-voice--icon");
                                a.textContent = "";
                                a.setAttribute("href", `https://voix.larousse.fr/${a.getAttribute("href").replace("/dictionnaires-prononciation/","").replace("/tts","")}.mp3`);
                                a.setAttribute("target", "_blank");
                                a.onclick = play;
                            })
                    })
                    return;
                },
                etymonline: function() {
                    let all = false,
                        section = lang == "" ? ".ant-row-flex" : ((all = true), ".entry-panel");
                    self._slice(all, dBody, section, fragment);
                },
                artfl: function() {
                    //if (!/rogets/.test(lang)) this._addFontFamily("Amiri|Open+Sans&amp;subset=arabic");
                    if (lang == "rogets") {
                        var index = 4,
                            table = dBody.querySelectorAll("table");
                        for (let tbl of table) {
                            tbl.style.width = null;
                        }
                        var strF = (i, txt) => {
                            return table[i].textContent.indexOf(txt) > -1;
                        };
                        if (/Amazon/.test(table[table.length - 3].textContent)) table[table.length - 3].remove();
                        while (
                            strF(index, "Search Results") ||
                            strF(index, "Mawson, C.O.S., ed. (1870–1938). Roget’s International Thesaurus. 1922.")
                        ) {
                            index++;
                        }
                        index++;
                        index = strF(index, "Search Results") ? index + 1 : index + 2;
                        table[index].style.width = "";
                        fragment.appendChild(table[index]);
                    } else if (lang == "dvlf") {
                        let self = korpus;
                        let json = JSON.parse(document.body.textContent);
                        if (json.dictionaries.data.length == 0) throw "I'm Evil";
                        var results_container = self._createElement(
                                                    "div", {
                                                        class: "results-dvlf"
                                                    },
                                                    dBody
                            ),
                            div = self._createElement(
                                "div", {
                                    class: "panel-default"
                                },
                                results_container
                            ),
                            dico_entries,
                            entry,
                            header,
                            synant,
                            example,
                            frag,
                            createEntry = function(result, div) {
                                if (result.contentObj[0]) {
                                    header = self._createElement(
                                        "div", {
                                            class: "dico-title"
                                        },
                                        div
                                    );
                                    header.textContent = result.label;
                                    dico_entries = self._createElement(
                                        "div", {
                                            class: "dico-entries"
                                        },
                                        div
                                    );
                                    frag = document.createRange().createContextualFragment(result.contentObj[0].content);
                                    dico_entries.appendChild(frag);
                                }
                            };
                        for (let result of json.dictionaries.data) {
                            createEntry(result, div);
                        }
                        for (let ts of ["synonyms", "antonyms"]) {
                            header = self._createElement(
                                "div", {
                                    class: "dico-title"
                                },
                                div
                            );
                            header.textContent = (str => str.charAt(0).toUpperCase() + str.slice(1))(ts);
                            synant = self._createElement(
                                "p", {
                                    class: ts
                                },
                                div
                            );
                            for (let t of json[ts]) {
                                entry = `<a href=\"http://dvlf.uchicago.edu/api/mot/${t.label}\" class=\"antonym\">${t.label},</a> `;
                                frag = document.createRange().createContextualFragment(entry);
                                synant.appendChild(frag);
                            }
                        }
                        header = self._createElement(
                            "div", {
                                class: "dico-title"
                            },
                            div
                        );
                        header.textContent = "Exemples d'utilisation";
                        example = self._createElement(
                            "div", {
                                class: "example-content"
                            },
                            div
                        );
                        for (let e of json.examples) {
                            dico_entries = self._createElement(
                                "div", {
                                    class: "example-content"
                                },
                                example
                            );
                            entry = e.content + " <cite> Source: " + e.source + "</cite>";
                            frag = document.createRange().createContextualFragment(entry);
                            dico_entries.appendChild(frag);
                        }
                        fragment.appendChild(results_container);
                        data.url = data.url.replace(/\/api/, "");
                    } else if (["frantext", "olddict"].includes(lang)) {
                         let entries = JSON.parse(dBody.textContent),
                             results_container = korpus._createElement(
                                "div", {
                                    class: "results-olddict"
                                },
                                dBody
                            ),
                            ol = korpus._createElement(
                                "ol", {
                                    class: "philologic_concordance"
                                },
                                results_container
                            ),
                            li,
                            citation,
                            context,
                            spans,
                            parser = new DOMParser(),
                            createEntry = function(result, ol, i) {
                                li = korpus._createElement(
                                    "li", {
                                        class: "philologic-occurrence panel panel-default"
                                    },
                                    ol
                                );
                                citation = korpus._createElement(
                                    "div", {
                                        class: "citation-container row"
                                    },
                                    li
                                );
                                let m = result.metadata_fields;
                                citation.textContent = `${m.head} [${m.year}] ${m.author} ${m.title}`;
                                context = korpus._createElement(
                                    "div", {
                                        class: "philologic_context text-content-area"
                                    },
                                    li
                                );
                                spans = parser.parseFromString(result.context.replace(/\&/gi, " "), "text/xml");
                                context.appendChild(spans.firstChild);
                            };
                        for (let result of entries.results) {
                            createEntry(result, ol);
                        }
                        fragment.appendChild(results_container);
                        data.url =
                            (data.lang == "frantext" ?
                                "http://artflsrv02.uchicago.edu/philologic4/frantext/" :
                                "https://artflsrv03.uchicago.edu/philologic4/publicdicos/") +
                            `query?report=concordance&method=proxy&q=${data.phrase}&start=0&end=0`;
                        fragment.appendChild(this._copyright(data));
                    } else if (lang.indexOf("perseus") > -1) {
                        let content = dBody.querySelector("#content"),
                            nodes = [].slice.call(content.childNodes);
                        for (let node of nodes) {
                            if (node.tagName == "HR") {
                                break;
                            }
                            node.remove();
                        }
                        content.classList.add("results-olddict");
                        fragment.appendChild(content);
                    } else if (lang.indexOf("logeion") > -1) {
                        //this._addFontFamily("Amiri");
                        var detail = JSON.parse(dBody.textContent).detail,
                            parser = new DOMParser(),
                            article,
                            part = function(tag, txt, child_nodes = false) {
                                let h = document.createElement(tag);
                                if (!child_nodes) h.textContent = txt
                                else {
                                    h.className = txt;
                                    child_nodes = parser.parseFromString(child_nodes, 'text/html').querySelector("body").childNodes;
                                    Array.from(child_nodes).forEach((elmt, index, obj) => h.appendChild(elmt));
                                }
                                article.appendChild(h);
                            };

                        article = document.createElement("article");
                        article.className = "logeion";

                        if (detail.dicos.length > 0) {
                            part("h3", detail.headword);
                            part("p", detail.shortdef);
                            for (var dico of detail.dicos) {
                                if (dico.dname != "LewisShort") {
                                    part("h3", dico.dname);
                                    part("p", dico.dname, dico.es[0])
                                }
                            }
                            var short = { bwl: "BWL", friezeD: "FriezeDennisonVergil", lewisshort: "LewisShort" }
                            for (var h of Object.keys(short)) {
                                if (detail.bwl.length > 0)
                                    part("h3", short[h]),
                                    part("p", h, detail[h][0]);
                            }
                        }
                        fragment.appendChild(article);
                        data.url = `http://logeion.uchicago.edu/${data.phrase}`;
                    }
                    return;
                },
                wordreference: function() {
                    if (/ar/.test(lang)) dBody.getElementsByTagName("html")[0].dir = "";
                    let header = dBody.querySelector("#articleHead");
                    if (header.querySelector("#headertext") || header.querySelector("#noEntryFound")) throw "I'm Evil";
                    self._slice(false, dBody, "#centercolumn", fragment);
                    header = fragment.querySelector("#headerTabs")
                    if (header) {
                        let ul = header.querySelector("ul"),
                            li = this._createElement("li", { id: "tabRev" }, ul),
                            a = this._createElement("a", {}, li);
                        (a.href = data.url.replace(data.lang, data.lang + "/reverse")),
                        (a.textContent = "WR Reverse"),
                        li.previousElementSibling.remove();
                    }
                    return;
                },
                linguee: function() {
                    if (/deepl/i.test(lang)) {
                        self._slice(false, dBody, "#dl_translator", fragment);
                        let st = fragment.querySelector(".lmt__sides_container"),
                            f = document.createDocumentFragment();
                        f.appendChild(st.querySelector(".lmt__side_container--source"));
                        st.appendChild(f);
                        var textarea = fragment.querySelector(".lmt__source_textarea");
                        textarea.focus();
                        setTimeout(() => {
                            textarea.focus();
                            textarea.value = data.phrase;
                            textarea.style.maxHeight = ((X, Y) => {
                                X = data.isMobile ? X : Y;
                                return X[0] * textarea.value.length + X[1] + "px";
                            })([0.31, 56], [0.31, 40]);
                            var evt = document.createEvent("Events");
                            evt.initEvent("change", true, true);
                            textarea.dispatchEvent(evt);
                        }, 500);
                    } else
                        self._slice(false, dBody, "#lingueecontent", fragment);
                    return;
                },
                woxikon: function() {
                    document.head.insertAdjacentHTML(
                        "beforeend",
                        '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">'
                    );
                    let url = window.location.href,
                        url0 = url.slice(0, url.indexOf("woxikon") + 10 + 1);
                    if (url0.indexOf("www.woxikon.com") > -1) {
                        self._slice(true, dBody, ".words-all-translations-item", fragment);
                    } else if (url0.indexOf("dict.woxikon.com") > -1) {
                        self._slice(false, dBody, "#content", fragment);
                    } else {
                        self._slice(false, dBody, ".synonyms-list-inner", fragment);
                        self._slice(false, dBody, ".synonyms-list", fragment);
                    }
                    return;
                },
                dictcc: function() {
                    self._slice(false, dBody, "#transview", fragment);
                    let audio = new Audio,
                        play = function() {
                            audio.currentSrc = this.getAttribute("href");
                            audio.play();
                            return false;
                        },
                        snd = Array.from(fragment.querySelectorAll(".audiobut")),
                        p;
                    if (snd.length > 0)
                        snd.forEach(a => {
                            a.classList.add("k-voice--icon");
                            p = a.getAttribute("onclick");
                            a.setAttribute("href", "https://audio.dict.cc/speak.audio.php?type=mp3&id=" +
                                p.match(/\d+/)[0] + "&lang=" +
                                p.match(/[A-Z]+/)[0] + "_rec_ip&lp=" + lang.toUpperCase());
                            a.onclick = play;
                            a.setAttribute("target", "_blank");
                        })
                    return;
                },
                leo: function() {
                    self._slice(false, dBody, ["#section-subst", "#section-verb", "#section-adjadv", "#section-phrase", "#section-example"], fragment);
                    var audio = new Audio();
                    fragment.querySelectorAll('.icon_play-circle-outline').forEach((i, j, o) => {
                        i.removeAttribute("onclick");
                        i.classList.add("k-icon--volumeup","k-voice--icon");
                        i.onclick = function() {
                            audio.src =
                                "https://dict.leo.org/media/audio/" + this.getAttribute("data-dz-rel-audio") + ".ogg";
                            audio.play();
                            return false;
                        };
                    })
                    return;
                },
                dictionarycom: function() {
                    let section = Array.from(dBody.querySelectorAll("h1")).filter(h => h.textContent.indexOf(data.phrase) > -1)[0];
                    section = section.parentElement.parentElement.parentElement.parentElement;
                    if (section) {
                        if (lang == "dictionary") {
                            let Arr = [],
                                tw = document.createTreeWalker(section, NodeFilter.SHOW_ELEMENT, null, false),
                                node;
                            tw.currentNode = section.firstElementChild;
                            node = tw.currentNode, node.firstElementChild.nextElementSibling.remove(), Arr.push(node);
                            node = tw.nextSibling(), node = tw.nextSibling(), node = tw.nextSibling(), Arr.push(node);
                            while (node && node.nodeName != "H2") { node = tw.nextSibling() };
                            while (node) {
                                Arr.push(node);
                                node = tw.nextSibling();
                            }
                            Arr.forEach(node => fragment.appendChild(node));
                            fragment.querySelectorAll(".audio-wrapper").forEach(s => {
                                s.classList.add("k-voice--icon");
                                s.onclick = function() {
                                    let audio = new Audio();
                                    audio.src = this.querySelector("audio").currentSrc;
                                    audio.play();
                                }
                            })
                        } else {
                            section = section.firstElementChild;
                            fragment.appendChild(section.firstElementChild);
                            fragment.appendChild(section.firstElementChild);
                        }
                    }
                    return;
                 },
                beolingus: function() {
                    self._slice(false, dBody, "#dresult1", fragment);
                    var as = [].slice.call(fragment.querySelectorAll("a"));
                    var audio = new Audio();
                    for (let a of as) {
                        a.removeAttribute("onclick");
                        a.removeAttribute("ondblclick");
                        if (/speak/.test(a.href)) {
                            a.classList.add("k-icon--volumeup","k-voice--icon");
                            a.onclick = function() {
                                audio.src = this.href
                                    .replace(/(dings\.cgi\?speak=)/gi, "speak-")
                                    .replace(/(;text\=\w*)/gi, ".mp3")
                                    .replace(/(%\d*{\w*})/gi, "");
                                audio.play();
                                return false;
                            };
                        } else {
                            a.onclick = function() {
                                window.location.href = this.href;
                                return true;
                            };
                        }
                    }
                    return;
                },
                thefreedictionary: function() {
                    self._slice(false, dBody, "#MainTxt", fragment);
                    let audio = new Audio(), n;
                    for (let cls of [".snd", ".snd2"])
                        fragment.querySelectorAll(cls).forEach(s => {
                            s.classList.add("k-voice--icon");
                            s.onclick = function() {
                                n = cls.match(/\d/);
                                audio.src = `https://img${n ? n[0] : ""}.tfd.com/${n ? "pron" : "hm"}/mp3/${s.getAttribute("data-snd")}.mp3`;
                                audio.play();
                            }
                        });                   
                    return;
                },
                urbandictionary: function() {
                    self._slice(true, dBody, ".def-panel", fragment);
                    var audio = new Audio();
                    fragment.querySelectorAll(".play-sound").forEach((a, i, o) => {
                        a.classList.add("k-voice--icon");
                        a.onclick = function() {
                            audio.src = a.getAttribute("data-urls").replace(/["\\\[\]]/g, "");
                            audio.play();
                            return false;
                        };
                    })
                    return;
                },
                memidex: function() {
                    self._slice(false, dBody, [], fragment);
                    return;
                },
                collins: function() {
                    //this._addFontFamily("Open+Sans");
                    self._slice(true, dBody, ".res_cell_center_content", fragment);
                    var audio = new Audio();
                    fragment.querySelectorAll(".icon-volume-up").forEach(a => {
                        a.classList.add("k-voice--icon");
                        a.onclick = function() {
                            audio.src = this.getAttribute("data-src-mp3");
                            audio.play();
                            return false;
                        };
                    });
                    return;
                },
                macmillan: function() {
                    //this._addFontFamily("Open+Sans");
                    self._slice(false, dBody, ["#headwordleft", "#headbar", "#relatedentries", "#leftContent"], fragment);
                    return;
                },
                oxforddictionaries: function() {
                    self._slice(false, dBody, ".entryWrapper", fragment);
                    let audio = new Audio();
                    fragment.querySelectorAll(".headwordAudio").forEach(a => {
                        a.classList.add("k-voice--icon");
                        a.onclick = function(){
                            audio.src = this.firstElementChild.src;
                            audio.play();
                        }
                    })
                    return;
                },
                merriamwebster: function() {
                    if (lang === 'websters1913') {
                        self._slice(false, dBody, ".result-content", fragment);
                    } else {
                        //this._addFontFamily("Open+Sans|Noto+Sans");
                        self._slice(false, dBody, ".left-content", fragment);
                        let audio = new Audio(),
                            play = function() {
                                audio.src = this.getAttribute("href");
                                audio.play();
                                return false;
                            };
                        fragment.querySelectorAll(".play-pron").forEach(a => {
                            let url = "https://media.merriam-webster.com/audio/prons/"
                            a.setAttribute("href", url + a.getAttribute("data-lang").replace("_", "/") + "/mp3/" + a.getAttribute("data-dir") + "/" + a.getAttribute("data-file") + ".mp3");
                            a.setAttribute("target", "_blank");
                            a.classList.add("k-voice--icon");
                            a.onclick = play;
                        })
                    }

                    return;
                },
                almaany: function() {
                    // this._addFontFamily("Amiri")
                    self._slice(true, dBody, ".panel-body", fragment);
                    this._removeElementAll(fragment, ["select", ".input-group-btn"]);
                    return;
                },
                iate: function() {
                    if (dBody.querySelector("#searchResultPage")) self._slice(false, dBody, "#searchResultPage", fragment);
                    else self._slice(true, dBody, "#entryDetailPage", fragment);
                    return;
                },
                eurovoc: function() {
                    if (dBody.querySelector(".portlet-boundary_portal2012SearchResult_WAR_portal2012portlet_")) {
                        self._slice(false, dBody, ".portlet-boundary_portal2012SearchResult_WAR_portal2012portlet_", fragment)
                    } else if (dBody.querySelector("#p_p_id_conceptdisplay_WAR_euvocportlet_")) {
                        self._slice(false, dBody, "#p_p_id_conceptdisplay_WAR_euvocportlet_", fragment);
                    }
                    [].slice.call(fragment.querySelectorAll("a")).map(a => a.removeAttribute("target"));
                    return;
                },
                pons: function() {
                    //this._addFontFamily("Open+Sans");
                    self._slice(false, dBody, ".results", fragment);
                    var play_audio = function() {
                        var lvar = ["fr_FR", "pt_PT", "es_ES"],
                            i = this,
                            id = (x => { while (x && x.tagName != 'DL') x = x.parentNode; return x.id })(i),
                            l = i.getAttribute('data-pons-lang') || lvar[lvar.findIndex(e => Object.keys(JSON.parse(i.getAttribute('data-variant'))).indexOf(e) > -1)];

                        var audio_url = `https://sounds.pons.com/audio_tts/${l.toLowerCase()}/${id}`,
                            audio_elementement = document.createElement("audio"),
                            mp3_supported = !!(
                                audio_elementement.canPlayType && audio_elementement.canPlayType("audio/mpeg;").replace(/no/, "")
                            );
                        if (mp3_supported) {
                            audio_elementement.setAttribute("src", audio_url);
                            audio_elementement.play();
                        }
                        return false;
                    };
                    fragment.querySelectorAll(".icon-volume-down").forEach( i=> {
                        i.classList.add("k-voice--icon");
                        i.addEventListener("click", play_audio, false);
                    });
                    return;
                },
                duden: function() {
                    self._slice(true, dBody, "section.vignette", fragment);
                    self._slice(true, dBody, "article[role^=article]", fragment);
                    return;
                },
                wortschatzunileipzig: function() {
                    self._slice(false, dBody, ".cnt.panel-body", fragment);
                    return;
                },
                dwds: function() {
                    document.head.insertAdjacentHTML(
                        "beforeend",
                        '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">'
                    );
                    self._slice(false, dBody, ".col-md-9", fragment);
                    this._removeElement(fragment, [".row"]);
                    this._removeElementAll(fragment, [".navbar"]);
                    fragment.querySelector(".col-md-10.col-md-offset-1").style = "";
                    fragment.querySelector(".sans").style = "";
                    [].slice
                        .call(fragment.querySelectorAll("a.intern"))
                        .map(
                            a => a.previousSibling && (a.previousSibling.textContent = a.previousSibling.textContent.replace("↗", ""))
                        );
                    return;
                },
                rae: function() {
                    console.log(lang)
                    let f = {
                        dle: ["#resultados"],
                        desen: ["p", "ul","table"],
                        damer: ["p", "ul","table"],
                        dpd: (window.location.href.includes("key") ? "ul" : "div")
                    };
                    if (Object.keys(f).indexOf(lang) > -1) self._slice(false, dBody, f[lang], fragment)
                    else if (/etim/.test(lang)) self._slice(false, dBody, "article", fragment)
                    else if (/lar/.test(lang)) self._slice(true, dBody, ".ficha-diccionario", fragment)
                    else self._slice(false, dBody, "#col-detalle-derecha", fragment);
                    return;
                },
                reverso: function() {
                    self._slice(false, dBody, "#top-results", fragment);
                    self._slice(false, dBody, "#examples-content", fragment);
                    fragment.querySelectorAll("input")[1].remove();
                    [].slice.call(fragment.querySelectorAll(".translation")).map(a => (a.title = ""));
                    return;
                },
                chambers: function() {
                    self._slice(false, dBody, "#fullsearchresults", fragment);
                    return;
                },
                googletranslate: function() {
                    var cleanCSS = function(tag) {
                        var elements = document.head.querySelectorAll(tag);
                        for (var i = 0; i < elements.length; i++) {
                            elements[i].parentNode.removeChild(elements[i]);
                        }
                    };
                    cleanCSS("link[rel=stylesheet]");
                    cleanCSS("style");
                    self._slice(false, dBody, ".homepage-content-wrap", fragment);
                    let st = fragment.querySelector(".source-target-row"),
                        f = document.createDocumentFragment();
                    f.appendChild(st.querySelector(".tlid-input.input"));
                    st.appendChild(f);
                    let textarea = fragment.querySelector("#source");
                    textarea.value = data.phrase;                    
                    setTimeout(() => {
                        textarea.focus();                        
                        textarea.style.height = textarea.scrollHeight + "px";
                        dBody.querySelectorAll("iframe").forEach(e=>e.remove());
                    }, 1000);
                    if (["ar", "fa", "ur", "he", "zh"].indexOf(lang) > -1) {
                        document.head.insertAdjacentHTML(
                            "beforeend",
                            '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Amiri|Open+Sans&amp;subset=arabic">'
                        );
                    }
                    return;
                },
                cedict: function() {
                    if (lang == "simplified" || lang == "traditional") {
                        self._slice(false, dBody, "table.wordresults", fragment);
                        let tts = e => {
                            e = e.target.parentElement.parentElement;
                            if (/pinyin|hanzi/.test(e.className)) {
                                let voices = e.parentElement.querySelector(".pinyin a").getAttribute("onclick").match(/\|(.*?)'/)[1].split(/\s/),
                                    url = i => `https://www.mdbg.net/chinese/rsc/audio/voice_pinyin_pz/${voices[i].replace("'", "")}.mp3`,
                                    audio = new Audio(),
                                    i = 0,
                                    play = function() {
                                        if (i == voices.length - 1) return;
                                        i = ++i < voices.length ? i : 0;
                                        audio.src = url(i)
                                        audio.play();
                                    }
                                audio.addEventListener('ended', play, true);
                                audio.src = url(0);
                                audio.play();
                            }
                        }
                        dBody.addEventListener("click", tts)
                    }
                    if (lang == "hanzidico") {
                        self._cedict(JSON.parse(dBody.textContent), fragment);
                        data.url = "http://www.hanzidico.com/dictionnaire-chinois-francais-anglais/?query=" + data.phrase;
                    }
                    if (lang == "jukuu") {
                        let t = dBody.querySelector("#Table1 td");
                        fragment.appendChild(t.querySelector("table"));
                        t = Array.from(t.nextElementSibling.children);
                        t.forEach(table=>fragment.appendChild(table))
                    }

                    if (lang == "linedict") {
                        self._slice(false, dBody, "#content_wrapper", fragment);
                    }

                    if (lang == "iciba") {
                        self._slice(false, dBody, ".js-base-info", fragment);
                    }

                    if (lang == "dvo") {
                        self._slice(false, dBody, "#leftnav", fragment);
                    }
                    return;
                },
                triantafyllides: function() {
                    data.lang = data.lang.replace(/\//g, "");
                    self._slice(false, dBody, "#colM", fragment);
                    this._removeElement(fragment, ["#toolbar", ".tags", ".basket", "#fpage"]);
                    return;
                },
                gramota: function() {
                    //this._addFontFamily("PT+Serif");
                    self._slice(false, dBody, ".block-content", fragment);
                    [].slice.call(fragment.querySelectorAll("a")).map(a => (a.href = ""));
                    return;
                },
                dsal: function() {
                    //this._addFontFamily("Amiri&amp;subset=arabic");
                    var nodes = [].slice.call(dBody.childNodes),
                        node;
                    for (node of nodes) {
                        if (node.tagName == "HR") {
                            break;
                        }
                        nodes = nodes.slice(1);
                        node.remove();
                    }
                    for (node of nodes) {
                        fragment.appendChild(node);
                    }
                    fragment.lastElementChild.remove();
                    fragment.lastElementChild.remove();
                    return;
                },
                farsidics: function() {
                    //this._addFontFamily("Amiri&amp;subset=arabic");
                    var nodes = [].slice.call(dBody.childNodes),
                        node;
                    for (node of nodes) {
                        fragment.appendChild(node);
                    }
                    return;
                },
                seslisozluk: function() {
                    self._slice(false, dBody, "#ss-section-left", fragment);
                    self._slice(false, dBody, "#ss-section-right", fragment);
                    return;
                },
                treccani: function() {
                    //this._addFontFamily("Crimson+Text");
                    if (lang == "crusca") {
                        self._slice(false, dBody, "#results", fragment);
                    } else {
                        self._slice(false, dBody, [".module-article-full_content", ".module-widget-vocabulary"], fragment);
                    }
                    return;
                },
                sapere: function() {
                    var section = "";
                    section = [].slice.call(dBody.querySelectorAll('*[id^="id_"]'));
                    section.pop();
                    for (var s of section) fragment.appendChild(s);
                    return;
                },
                corriere: function() {
                    if (data.lang) {
                        self._slice(false, dBody, ".chapter", fragment);
                    } else {
                        self._slice(true, dBody, ".hentry--item__content", fragment);
                    }
                    return;
                },
                jmdict: function() {
                    let tag = lang == "jmdict" ? "#dictEntries" : "#contentBody";
                    self._slice(false, dBody, tag, fragment);
                    [].slice.call(fragment.querySelectorAll("script")).map(s => s.remove());
                    if (tag == "#dictEntries") {
                        [].slice.call(fragment.querySelectorAll("span.writing")).map(span => {
                            let arr = (str => {
                                    let a = [];
                                    for (let s of str) a.push(s);
                                    return a;
                                })(span.textContent),
                                a;
                            span.textContent = "";
                            for (let k of arr) {
                                a = document.createElement("a");
                                a.className = "stlh";
                                a.href = "kanji/" + k;
                                a.textContent = k;
                                span.appendChild(a);
                            }
                        });
                    }
                    return;
                },
                priberam: function() {
                    let tag = data.lang == "dicionarios" ? "#resultados" : ".pb-main-content";
                    self._slice(false, dBody, tag, fragment);
                    var sp;
                    fragment.querySelectorAll(".word").forEach((s, i, o) => {
                        sp = s.cloneNode(true);
                        s.parentNode.replaceChild(sp, s)
                    })
                    return;
                },
                michaelis: function() {
                    self._slice(false, dBody, "#content", fragment);
                    var sp;
                    for (tag of ["rg", "cg", "i"])
                        fragment.querySelectorAll(tag).forEach((s, i, o) => {
                            sp = s.cloneNode(true);
                            s.parentNode.replaceChild(sp, s)
                        });
                    return;
                },
                moedict: function() {
                    var section = dBody.querySelector("#result");
                    fragment.appendChild(section.querySelector(".title"));
                    fragment.appendChild(section.querySelector(".translation"));
                    fragment.appendChild(section.querySelector(".entry"));
                    return;
                },
                norstedts: function() {
                    //this._addFontFamily("PT+Serif");
                    self._slice(false, dBody, ".dictionary-search-result-container", fragment);
                    return;
                },
                spanishdict: function() {
                    var section = dBody.querySelectorAll(".dictionary-entry");
                    for (var s of section) fragment.appendChild(s);
                    var sp;
                    fragment.querySelectorAll(".has-tooltip").forEach((s, i, o) => {
                        sp = s.cloneNode(true);
                        s.parentNode.replaceChild(sp, s)
                    })
                    return;
                },
                wiktionary: function() {
                    self._slice(false, dBody, ".disambig-see-also", fragment);
                    self._slice(false, dBody, ".mw-parser-output", fragment);
                    data.url = data.url.replace(".m.",".")
                    let audio = new Audio();
                    fragment.querySelectorAll(".mediaContainer").forEach(a => {
                        a.classList.add("k-voice--icon");
                        a.onclick = function(){
                            audio.src = this.firstElementChild.currentSrc;
                            audio.play();
                        }
                    })
                    return;
                }
        },
        _noscript(fragment) {
            var ns = document.createElement("noscript");
            ns.textContent = "Your browser does not support JavaScript!";
            ns.type = "text/javascript";
            return ns;
        },
        _appendContainer(main, content, dictionary) {
            if (main)
                while (main.firstChild) {
                    main.removeChild(main.firstChild);
                }
            var containr = this._createElement(
                "div", {
                    class: "k-result--container"
                },
                main
            );
            content = this._createElement(
                "div", {
                    class: "k-result"
                },
                containr
            );
            return content;
        },
        _appendSearch(){
            let inpt = document.createElement("input");

            function search(e) {
                if (e.keyCode === 13) {
                    let phrase = inpt.value.trim().replace(/[~$%^&*_|¯<\=>\\^]/gi, "");
                    if (phrase) {
                        Object.assign(data, {
                            cmd: "search__phrase",
                            phrase: phrase
                        });
                        inpt.removeEventListener("keydown", search);
                        let channel = new MessageChannel();
                        window.parent.postMessage(data, "*", [channel.port2]);
                        inpt.value = "";
                        dBody.style.display = "none";
                    }
                }
            }
            inpt.className = "k-search-phrase";
            inpt.dir = "auto";
            inpt.placeholder = chrome.i18n.getMessage("placeholder_search");
            inpt.autofocus = "";
            inpt.type = "text";
            inpt.value = "";
            inpt.addEventListener("keydown", search);
            return inpt;
        },
        _appendNav(){
            let nav = document.createElement("div");
            nav.className = "k-dicts";
            nav.style.backgroundImage = "url(" + chrome.runtime.getURL("icons/stackvoicedictionary.svg") + ")";
            nav.addEventListener("click", function(e) {
                    nav.children[0]
                        ? nav.children[0].remove()
                        : nav.appendChild(this._appendDicts(data.rank, nd));
                }.bind(this)
            );
            return nav;
        },
        _appendDicts(r, m) {
            let self = this,
                d = ["WordReference", "Linguee", "GoogleTranslate", "EtymOnline", "CNRTL", "Duden", "OxfordDictionaries", "Almaany", "Wiktionary", "DictCC", "PONS", "LEO", "Littre", "Larousse", "Chambers", "Collins", "DictionaryCom", "MerriamWebster", "MacMillan", "DWDS", "ARTFL", "Beolingus", "Corriere", "Sapere", "Priberam", "Michaelis", "RAE", "Reverso", "TheFreeDictionary", "SpanishDict", "Woxikon", "CEDict", "JMDict", "DSAL", "Gramota", "Norstedts", "Memidex", "Triantafyllides", "MOEDict", "BabLa", "SesliSozluk", "FarsiDics", "EuroVoc", "IATE", "UrbanDictionary", "Treccani", "WortschatzUniLeipzig", "WebSpeechAPI", "ResponsiveVoiceOrg", "ISpeech","Watson"],
                sortIDs = function(d, r) {
                    let m = new Map(Object.entries(self.dictionaries)),
                        M = new Map();

                    r = r || (() => { r = Array.apply(null, { length: d.length + 1 }).map(Number.call, Number); r.shift(); return r; })()

                    for (let n in r)
                        M.set(n, [d[r[n] - 1], m.get(d[r[n] - 1].toLowerCase())[1]]);

                    M.forEach((v, k, m) => {
                        if (v[0].toLowerCase() == data.dictionary || !v[1]) M.delete(k)
                    })
                    
                    return Array.from(M).map(e => e[1][0])
                },
                IDs = sortIDs(d, r).slice(0, m),
                ul = document.createElement("ul"),
                li;

            IDs.splice(IDs.indexOf(data.dictionary), 1);
            ul.className = "k-dicts--more";

            for (let ID of IDs) {
                li = document.createElement("li");
                li.appendChild(document.createTextNode(ID));
                ul.appendChild(li);
            }
            ul.addEventListener(
                "click",
                function(e) {
                    if (e.target && e.target.matches("li")) {
                        Object.assign(data, {
                            dictionary: e.target.textContent.toLowerCase(),
                            cmd: "search__phrase__dict"
                        });
                        let channel = new MessageChannel();
                        window.parent.postMessage(data, "*", [channel.port2]);
                        ul.remove();
                        dBody.style.display = "none";
                    }
                }, { once: true }
            );
            return ul;
        },
        _addFontFamily(names) {
            let lnk = document.createElement("link")
            lnk.rel = 'stylesheet';
            lnk.type = 'text/css';
            lnk.href = `https://fonts.googleapis.com/css?family=${names}`; 
            document.head.appendChild(lnk);
        },
        _slice(all, body, tag, fragment) {
            var section = typeof tag === "string" ? "" : [];
            section =
                typeof tag === "string" ?
                body["querySelector" + (all ? "All" : "")](tag) :
                (tag.forEach((t, i, o) => {
                        t = body.querySelector(t);
                        t && section.push(t);
                    }),
                    section);
            this._appendSection(section, fragment);
        },
        _appendSection(section, fragment) {
            if (!section || section.length == 0) {
                /*section = document.createElement("section");
                section.appendChild(document.createTextNode("..."));
                fragment.appendChild(section);*/
            } else {
                Array.prototype.isPrototypeOf(section) ||
                    NodeList.prototype.isPrototypeOf(section) ?
                    section.forEach((s, i, o) => fragment.appendChild(s)) :
                    fragment.appendChild(section);
            }
        },
        _removeElement(fragment, a) {
            a.map(function(el) {
                fragment.querySelector(el) != null ? fragment.querySelector(el).remove() : "";
            });
        },
        _removeElementAll(fragment, e) {
            var df = fragment.querySelectorAll(e);
            for (var i = 0; i < df.length; i++) {
                df[i].parentNode.removeChild(df[i]);
            }
        },
        _createElement(tag, attrs, parent) {
            if (!attrs) attrs = {};
            var elm = document.createElement(tag);
            for (var i in attrs) {
                elm.setAttribute(i, attrs[i]);
            }
            if (parent) parent.appendChild(elm);
            return elm;
        },
        _baseUrl(base, urlR) {
            var a = dBody.querySelectorAll("a[href^= '" + urlR + "']");
            for (var i = 0; i < a.length; i++) {
                a[i].setAttribute("href", base + urlR);
            }
        },
        _cedict(json_, fragment) {
            let res = json_.ResultSet.Dico[0],
                table = document.createElement("table"),
                tbody = document.createElement("tbody"),
                tr = document.createElement("tr"),
                cell = (p, g, c, t) => {
                    let e = document.createElement(g);
                    e.className = c;
                    e.textContent = t;
                    p.appendChild(e)
                },
                td = (tr, c, A, element) => {
                    let t = document.createElement("td");
                    t.className = c;
                    for (let j of A) {
                        cell(t, "span", "", element[j]);
                    }
                    tr.appendChild(t);
                }

            ["Chinois - 中文", "Pīnyīn - 拼音 / Zhùyīn - 注音", "Def. - 定义"].forEach((e, i, a) => cell(tr, "th", "", e));
            tbody.appendChild(tr);

            for (let i of ["Exact", "Begin", "Content", "Charsdefcontent", "Wordsdefcontent"]) {
                if(res[i])
                    res[i].forEach(function(element, index, array) {
                        let trow = document.createElement("tr");
                        trow.className = "tablerow";
                        td(trow, "zh", ["SIMP", "TRAD"], element);
                        td(trow, "py", ["PINYIN", "ZHUYIN"], element);
                        td(trow, "def", ["DEF_FR", "DEF_EN"], element);
                        tbody.appendChild(trow);
                    }) 
            }
            table.appendChild(tbody);
            table.className = "dicoTable";
            fragment.appendChild(table);
        },
        _copyright(data) {
            data.url = data.url.replace("https://cors-anywhere.herokuapp.com/", "");
            var blckq = document.createElement("blockquote"),
                p = document.createElement("p"),
                a = document.createElement("a"),
                bs = document.createTextNode(data.url.match(/https?:\/\/[\w.-]*/gi)[0]),
                wd = document.createTextNode("/../" + data.phrase),
                lr = document.createElement("span"),
                lfc = document.createElement("span"),
                lc = document.createElement("span");
            blckq.className = "k-cpright";
            a.href = data.url;
            a.target = "_blank";
            a.rel = "noopener noreferrer";
            lr.appendChild(bs);
            lr.className = "lien_racine";
            lfc.appendChild(wd);
            lfc.className = "lien_fin_coupee";
            lc.appendChild(lr);
            lc.appendChild(lfc);
            lc.className = "lien_court";
            a.appendChild(lc);
            p.textContent = "Source: ";
            p.appendChild(a);
            blckq.appendChild(p);
            return blckq;
        },
        _appendInline(h,n){
            if(data.console) return;
            let el, div = document.createElement("div"),
            span = t => {
                el = document.createElement("span");
                el.textContent = chrome.i18n.getMessage(t) || t;
                div.appendChild(el);
            }, rtl = /ar|he/.test(navigator.language || navigator.userLanguage);
            
            if(!rtl) span("translate");
            span("inline"), el.className = "k-inline";
            if(/googletranslate/.test(data.dictionary)){
                span(" | ");               
                span("entire_page"), el.className = "k-inline", el.id = "k-all";
            }
            if(rtl) span("translate");
            
            h.insertBefore(div, n); 
            div.addEventListener("click", function(e) {
                    let channel = new MessageChannel();
                    window.parent.postMessage({ cmd: "inline_translate", all: e.target.id === "k-all" }, "*", [channel.port2]);                }
            );
        },
        _inlineTranslate(data){
            let channel = new MessageChannel(),
                main;

            window.parent.postMessage({ cmd: "translation_wrapp" }, "*", [channel.port2]);

            channel.port1.onmessage = function(e) {
                let onChange = function(m, s, r, o) {
                    let val = r => r.nodeName === "TEXTAREA" ? r.value : typeof(r) === "string" ? r : r.textContent,
                        result, r0, i = 0,
                        channel = new MessageChannel(),
                        _send = (m, t) => window.parent.postMessage({ cmd: m, translation: t }, "*", [channel.port2]),
                        error = t => {clearInterval(t), _send("translation_error", "")};

                    try {
                        m.querySelector(s).value = data.phrase_norm;
                        let st = setInterval(() => {
                            result = m.querySelector(r);
                            if (result)
                                if (val(result).match(/\w/g)) {
                                    clearInterval(st);
                                    if (o) {
                                        result = m.querySelector(r).querySelector(o);
                                        result = Array.from(result.childNodes)
                                            .map(e => {
                                                return e.nodeName == "BR" ? "\n" : e.nodeName == "SPAN" ? e.firstChild.data : "";
                                            }).join("").replace(/(\n){2,}/g, "\n").replace(/\./g, ". ");
                                    } else
                                        result = m.querySelector(r);
                                    _send("translation_completed", val(result))
                                };
                            if (++i > 12) error(st)
                        }, 500)
                    } catch (e) {
                        _send("translation_error", "");
                    }
                };

                data.phrase_norm = e.data.phrase_norm;
                if (/googletranslate/.test(data.dictionary)) {
                    main = document.body.querySelector(".source-target-row");
                    onChange(main, "textarea#source", ".result-shield-container", ".tlid-translation.translation")
                } else if (/deepl/i.test(lang)) {
                    main = document.body.querySelector(".lmt__sides_container");
                    onChange(main, ".lmt__source_textarea", ".lmt__textarea.lmt__target_textarea", 0);
                }
            };        
        }
    };

    korpus = new _();

    document.addEventListener("DOMContentLoaded", korpus.onDOMContentLoaded);
    window.addEventListener("beforeunload", function(e) {
        let channel = new MessageChannel();
        window.parent.postMessage({
                cmd: "hash__changed"
            },
            "*", [channel.port2]
        );
    });
}