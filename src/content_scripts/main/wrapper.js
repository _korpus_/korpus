"use strict";

function Utils() {
    var self = this;
    this.DEFAULT_LEAF_BLACKLIST_SELECTOR = "SUP, SUB, SCRIPT, STYLE, IMG, SVG".split(/,\s+/);
    this.DEFAULT_ROOT_WHITELIST_SELECTOR = "H1, H2, H3, H4, H5, H6, P, TH, TD, CAPTION, LI, BLOCKQUOTE, Q, DT, DD, FIGCAPTION".split(/,\s+/);
    this.selectorFilterText = {
        acceptNode: function(node) {
            if (/\S/.test(node.nodeValue)) {
                if (self.leafExcludeFilter(node.parentNode)) {
                    return NodeFilter.FILTER_REJECT;
                }
                return NodeFilter.FILTER_ACCEPT;
            }
            return NodeFilter.FILTER_SKIP;
        }
    };
    this.selectorFilterElement = {
        acceptNode: function(node) {
            if(self.leafExcludeFilter(node)){
                return NodeFilter.FILTER_REJECT;
            }

            if (self.rootIncludeFilter(node)) {                    
                    return NodeFilter.FILTER_ACCEPT;
            }            
            return NodeFilter.FILTER_SKIP;
        }
    };
    this.containerRef = null;
}

Utils.prototype = {
    leafExcludeFilter(element) {
        return this.DEFAULT_LEAF_BLACKLIST_SELECTOR.indexOf(element.nodeName) > -1;
    },

    rootIncludeFilter(element) {
        return this.DEFAULT_ROOT_WHITELIST_SELECTOR.indexOf(element.nodeName) > -1;
    },
    
    get selection() {
        return window.getSelection();
    },

    get anchorFocus() {
        var t, u,
            anchorNode = "anchorNode",
            focusNode = "focusNode",
            anchorOffset = "anchorOffset",
            focusOffset = "focusOffset",
            selection = this.selection;

        var anchorNodePosition = selection[focusNode].compareDocumentPosition(selection[anchorNode]);
        if (
            /4|16/.test(anchorNodePosition) ||
            (/0/.test(anchorNodePosition) && selection[anchorOffset] > selection[focusOffset])
        ) {
            t = focusNode, u = focusOffset;
            focusNode = anchorNode, focusOffset = anchorOffset;
            anchorNode = t, anchorOffset = u;
        }

        return { anchorNode, anchorOffset, focusNode, focusOffset }
    },

    get paragraphs() {

        var CONT = [],
            startOffset, endOffset,
            hasWhitelistSelector,

        _ = this.anchorFocus,
        selection = this.selection,
        
        cAC = selection.getRangeAt(0).commonAncestorContainer,
        validCurrentNode = W => {
            let e = W.currentNode;
            if (!this.rootIncludeFilter(e))
                while (e) {
                    e = W.nextNode();
                    if (e && e.contains(selection[_.anchorNode])) break;
                };
            return e
        };
        
        this.containerRef =
            cAC == selection.anchorNode ||
            cAC == selection.focusNode  ||
            wrapper.DEFAULT_MARKUP_BLACKLIST_SELECTOR.includes(cAC.nodeName)
                ? cAC.parentNode
                : cAC;

        var WALKER = document.createTreeWalker(
                this.containerRef, document.defaultView.NodeFilter.SHOW_ELEMENT, this.selectorFilterElement, false),
            element = validCurrentNode(WALKER), element_clone, o;

        if (element) {
            while (element && (this.isBefore(element, _.focusNode)||this.contains(element, _.focusNode))) {
                if (this.contains(element, _.anchorNode) || this.contains(element, _.focusNode)) {
                    startOffset = 0, endOffset = 0;
                    var tw = document.createTreeWalker(
                            element, document.defaultView.NodeFilter.SHOW_TEXT, this.selectorFilterText, false),
                        node = tw.nextNode();

                    while (node) {
                        o = this.calcOffset(selection, node, _, startOffset, endOffset);
                        startOffset = o.startOffset, endOffset = o.endOffset;
                        node = tw.nextNode();
                    }

                    element_clone = element.cloneNode(true);
                    CONT.push({ element, startOffset, endOffset, element_clone });

                } else {
                    if (Array.from(element.childNodes).filter(n => this.rootIncludeFilter(n)).length > 0)
                        this.dig(element, WALKER, CONT)
                    else this.calc(element, CONT)
                }

                if (element.contains(selection[_.focusNode])) break;
                element = WALKER.nextSibling();
            }
        } else {
            WALKER = document.createTreeWalker(
                this.containerRef, document.defaultView.NodeFilter.SHOW_TEXT, this.selectorFilterText, false);
            element = validCurrentNode(WALKER);

            startOffset = selection[_.anchorOffset]
            while (element) {
                endOffset = element.contains(selection[_.focusNode]) ? selection[_.focusOffset] : element.textContent.length;
                CONT.push({ element: element.parentNode, startOffset: startOffset, endOffset: endOffset, element_clone: element.parentNode.cloneNode(true) });
                if (element.contains(selection[_.focusNode])) break;
                element = WALKER.nextNode();
                startOffset = 0;
            }
        }

        return new Promise(r => r(CONT));
    },

    isBefore(element, node) {
        return /4|16|20/.test(element.compareDocumentPosition(this.selection[node]));
    },

    contains(element, af) {
        return element.contains(this.selection[af]);
    },

    calcOffset(selection, node, _, startOffset, endOffset, ) {
        startOffset += this.contains(node, _.anchorNode) ?
            selection[_.anchorOffset] :
            this.isBefore(node, _.anchorNode) ?
            node.data.length : 0;

        endOffset += this.contains(node, _.focusNode) ?
            selection[_.focusOffset] :
            this.isBefore(node, _.focusNode) ?
            node.data.length : 0;

        return { startOffset, endOffset }
    },

    dig(element, WALKER, CONT) {
        let childs = Array.from(element.childNodes).filter(e => {
            return e.nodeType==Node.TEXT_NODE && /\S/g.test(e.data) ||
            !this.leafExcludeFilter(e)
        })
        let  tw = document.createTreeWalker(
                WALKER.currentNode, document.defaultView.NodeFilter.SHOW_ELEMENT, utils.selectorFilterElement, false);
        if (childs.length > 0) {
            for (let node of childs) {
                if (this.rootIncludeFilter(node)) {
                    tw.currentNode = node;
                    if (Array.from(node.childNodes).filter(n => this.rootIncludeFilter(n)).length > 0)
                        this.dig(node, tw, CONT)
                    else this.calc(node, CONT)
                }
            }
        }
    },

    calc(element, CONT) {
        let startOffset = 0,
            endOffset = 0,
            o;
        let tw = document.createTreeWalker(element, document.defaultView.NodeFilter.SHOW_TEXT, this.selectorFilterText, false),
            node = tw.nextNode();
        let selection = this.selection,
        _ = this.anchorFocus;
        while (node) {
            o = this.calcOffset(selection, node, _, startOffset, endOffset);
            startOffset = o.startOffset, endOffset = o.endOffset;
            node = tw.nextNode();
        }
        let element_clone = element.cloneNode(true);
        CONT.push({ element, startOffset, endOffset, element_clone});
    }
}

function Wrapp() {
    this.DEFAULT_MARKUP_BLACKLIST_SELECTOR = "A, ABBR, B, EM, FONT, I, MARK, SAMP, SMALL, STRONG, SPAN, STRIKE, KEYBOARD, VAR".split(/,\s+/);
    this.m = new Map();
    this.startOffset = 0;
    this.endOffset = 0;

    window.addEventListener(
        "message",
        function(e) { if (e.data) wrapper.onMessage(e) }.bind(this),
        false
    );
}

Wrapp.prototype = Object.create(Utils.prototype);

Wrapp.prototype = {
    surround(paragraph, h) {
        let r = document.createRange(),
            f = document.createElement('span'),
            node = paragraph.element;
        f.setAttribute("style", "vertical-align: inherit;");
        if (node.nodeType != Node.TEXT_NODE ) {
            if (node.childElementCount > 0) {
                let tN = document.createTextNode(node.textContent);
                while (node.firstChild) {
                    node.firstChild.remove()
                }
                node.appendChild(tN)
            }
            node = node.firstChild;
            r.selectNodeContents(node);
        };
        r.setStart(node, paragraph.startOffset);
        r.setEnd(node, paragraph.endOffset);
        r.surroundContents(f);
        this.clearSelection();
        h && this.highlight(f);
        paragraph.element = node.parentNode;
    },

    chunkify(text, CHARLIMIT = 500) {
        var punctuation = /[!?]+/,
            dot_not_acronym = /[a-zA-Z\u00C0-\u017F](?:[a-zA-Z\u00C0-\u017F]\.)/, 
            g = Math.min.apply(null, [text.search(punctuation), text.search(dot_not_acronym) + 1].filter(t => t > -1)) + 1, 
            e;
        g = g > 1 ? g : (text.length - 1)
        e = text.substr(0, g + 1);
        text = text.substr(e.length, text.length - e.length);

        return { chunk: e, text: text }
    },

    normalize(paragraph) {
        var node = paragraph.firstChild,
            node_next, S,
            swap = (node, S) => {
                let n = document.createTextNode(S ? node.textContent : "");
                node.replaceWith(n)
            };
        while (node) {
            S = /\S/g.test(node.textContent);
            node_next = node.nextSibling;
            if (!S) swap(node, S)
            if (node.nodeType === Node.ELEMENT_NODE)
                if (wrapper.DEFAULT_MARKUP_BLACKLIST_SELECTOR.includes(node.nodeName)) {
                    swap(node, S)
                };
            node = node_next
        }
        paragraph.normalize();
    },

    highlight(el){
            el.classList.add("k-wrapp-highlight");
    },

    clearSelection() {
        let selection = window.getSelection();
        if (selection.rangeCount > 0)
            selection.empty
                ? selection.empty()
                : selection.removeAllRanges && selection.removeAllRanges();
    },

    split(A) {
        let C = [];
        for (let t of A) {
            let c = this.chunkify(t);
            while (/\S/.test(c.chunk) && c.chunk.length > 1) {
                C.push(c.chunk)
                c = this.chunkify(c.text)
            }
        }
        return C;
    },

    nodeToText(paragraphs, mark) {
        return Array.from(paragraphs)
            .map(p => {
                p = p[1].element.querySelector("span").firstChild.data
                        .replace(/[\r\n\u21b5]/g, " ")
                        .replace(/\s{2,}/g, " ").trim() +
                        (/H1|H2|H3|H4|H5|H6/.test(p[1].element.nodeName) ? "." : "");
                p = p.replace(/(^.)/, " $1").replace(/(.$)/, "$1 ");

                return p;            
            })
            .join(mark);
    },

    textToNode(paragraphs, text, mark) {
        mark = text.indexOf(mark) > -1 ? mark : "\n";
        let B = text.split(mark).filter(m=>m), i= 0;
        for(let p of paragraphs) {
            B[i] = /H1|H2|H3|H4|H5|H6/.test(p[1].element.nodeName) ? B[i].replace(/(\.[\s]*)$/, "") : B[i];
            B[i] = B[i].replace(/(\?)/g,"$1 ").replace(/(\!)/g,"$1 ");
            p = p[1].element.querySelector("span");
            this.highlight(p);
            p.textContent = B[i];
            i++
        }
    },

    original(paragraphs){
        for(let p of paragraphs) {
            if(p.length > 1) p = p[1];
            p.element.replaceWith(p.element_clone);
        }
    },
    
    onMessage(e) {
        if (e.data.cmd === "translation_wrapp") {
            wrapp().then(r=>{
              e.ports[0].postMessage({ phrase_norm: text_norm });  
            });        
        } else if (e.data.cmd === "translation_completed"){
            if(/\S/g.test(e.data.translation)) wrapper.textToNode(paragraphs, e.data.translation, "\n")   
        } if (e.data.cmd === "translation_error") {
            wrapper.original(paragraphs)
        }
    }
}

async function init(utils, wrapper, h) {
    var P = new Map(),
        PARAGRAPHS = await utils.paragraphs, j = 0;
        for (let paragraph of PARAGRAPHS){
            wrapper.normalize(paragraph.element);
            wrapper.surround(paragraph, h);
            P.set("P" + j, paragraph);
            j++;      
        }

    return P
}

async function wrapp() {
    paragraphs = await init(utils, wrapper, true);
    text_norm = wrapper.nodeToText(paragraphs, "\n\n"); 
}

var utils = new Utils,
    wrapper = new Wrapp(),
    paragraphs, text_norm;