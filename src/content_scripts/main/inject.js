"use strict";
var data = {}, 
    prefs = {},
    lang, 
    dictionary,
    storage = chrome.storage.local,
    console_element = null,
    menu_el = null,
    cstyle,
    timer,
    gt = new Set(),
    rpt = false; 

       
    var $q = function(expr, con) {
        return typeof expr === "string" ? (con || document.body).querySelector(expr) : expr || null;
    };

    var $$q = function(expr, con) {
        return [].slice.call((con || document.body).querySelectorAll(expr));
    };

    function App() {

        this.isMobile = (M => /(android|bb\d+|meego).+mobile|\u0008(crios|chrome)(?:.+)mobile|bada\/|blackberry|playbook|bb10|blazer|compal|elaine|fennec|hiptop|iemobile|iphone|ip[oa]d|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|mobile safari|webos|mobile|tablet|\bcrmo\/|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(M && M.userAgent))(typeof window !== "undefined" && window.navigator ? window.navigator : null);

        this.keyCodes = { 16: "shift", 17: "ctrl", 18: "alt", 37: "left arrow", 38: "up arrow", 39: "right arrow", 40: "down arrow", 51: "3quote", 60: "<", 65: "a", 81: "q", 83: "s", 84: "t", 90: "z", 160: "^", 163: "#", 224: "meta" };

        this.container = "";

        this.selection = {};

        window.addEventListener(
            "message",
            function(e) {
                if (e.data) {
                    if (/DOM__content__cleaned|translation_completed|translation_error/.test(e.data.cmd)) {
                        menu_el.classList.remove("k-menu--easeIn");
                        app.menu_busyIndicator(false);
                    } else if (e.data.cmd === "inline_translate") {
                        prefs.options.extras.inlineTranslate = data.console ? "" : e.data.all ? "all" : "partial";                        storage.set({ options: prefs.options });
                    } else if ( e.data.cmd === "translation_wrapp") {
                        menu_el.classList.add("k-menu--easeIn");
                        app.menu_busyIndicator(true);
                    }
                }
            }.bind(this),
            false
        );

        window.addEventListener("resize", function(e) {
            if(console_element) menu_el.style = this.console_computeStyle();
        }.bind(this));

        storage.get(["dictionaries", "dictionary", "options", "cors", "isMobile"], r => {
            this._updates(r);
            document.addEventListener("DOMContentLoaded", this._init);
            if (document.readyState !== "loading") this._init();
        });

        chrome.storage.onChanged.addListener((ps, ns) => {
            Object.entries(ps).forEach(([key, value]) => (prefs[key] = value.newValue));
            this._hide();
            this._updates(prefs);
        });
    }

    App.prototype = {
        _on(event, handler, a, element) {
            element = element || document.body;
            event.split(/\s+/).forEach(function(e) {
                element[(a ? "add" : "remove") + "EventListener"](e, handler, false);
            });
        },
        _init() {
            app.mouse_load();
            document.removeEventListener("DOMContentLoaded", app._init);
        },
        _getElements(selector, cb, con) {
            var selectors = $$q(selector, con || console_element);
            [].forEach.call(selectors, cb);
        },
        menu_create(tag, attrs, parent) {
            if (!attrs) attrs = {};
            var el = document.createElement(tag);
            for (var i in attrs) {
                el.setAttribute(i, attrs[i]);
            }
            if (parent) parent.appendChild(el);
            return el;
        },
        menu_render() {
            menu_el = this.menu_create(
                "div", {
                    class: "k-menu",
                    unselectable: "on"
                },
                document.body
            );
            this.menu_create(
                "div", {
                    class: "k-menu--icons k-menu--dictionary",
                    title: "Define"
                },
                menu_el
            );
            this.menu_create(
                "div", {
                    class: "k-menu--icons k-menu--speak",
                    title: "Speak"
                },
                menu_el
            );
            this.menu_create(
                "div", {
                    class: "k-menu--icons k-menu--console",
                    title: "Console"
                },
                menu_el
            );
            this.menu_create(
                "div", {
                    class: "k-menu--busyIndicator"
                },
                menu_el
            );
            menu_el.querySelectorAll(".k-menu--icons").forEach(
                (e, i, o) => e.style.backgroundImage = "url(" + chrome.runtime.getURL("icons/stackvoicedictionary.svg") + ")"
            );
            this.menu_checkIcons();

            setTimeout(() => {
                let menu_el = document.body.querySelectorAll(".k-menu");
                if (menu_el.length > 1) menu_el[1].remove();
            }, 2000)
        },
        menu_load() {
            if (menu_el) return;
            this.menu_render();
            menu_el.addEventListener("click", this._show);            
        },
        menu_unload() {
            if (menu_el && menu_el.parentNode) menu_el.parentNode.removeChild(menu_el);
            window.clearTimeout(timer);
        },
        menu_move(e) {
            app.menu_load();
            if (!app.isMobile)
                Object.assign(menu_el.style,{
                        left: Math.max(e.clientX + window.scrollX + 3, 20) + "px",
                        top: Math.max(e.clientY + window.scrollY - 40, 20) + "px",
                        position: "absolute"
                    }
                );
            menu_el.classList.add("k-menu--easeIn");
            $$q(".k-menu--icons", menu_el).map(f => (f.style.opacity = 0.9));
            app.menu_busyIndicator(false);
            window.setTimeout(app.menu_hide, app.isMobile ? 5000 : 2000);
        },
        menu_hide() {
            if (menu_el) {
                menu_el.classList.remove("k-menu--easeIn");
            }
        },
        menu_checkIcons() {
            if (menu_el) {
                let j = 0;
                ["console", "speak"].forEach((elmt, index, obj) => {
                    if (prefs.options.extras[elmt]) j++;
                    $q('.k-menu--' + elmt, menu_el).style.display = prefs.options.extras[elmt] ? "block" : "none";
                })
                if (this.isMobile)
                    menu_el.style.top = 86 - j * 6 + "vh";
            }
        },
        menu_busyIndicator(f = true) {
            if (f) $$q(".k-menu--icons", menu_el).map(f => (f.style.opacity = 0));
            $q(".k-menu--busyIndicator", menu_el).classList.toggle("k-menu--busyIndicator_active", f)
        },
        mouse_load() {
            let modifierKey = prefs.options.hotkeys.modifier;

            this._on(
                modifierKey == "none" ? "touchstart mousedown" : "keydown",
                this.mouse_down,
                true
            );

            this._on("touchend mouseup", this.mouse_up, true);

            if (modifierKey != "none")
                this._on("keyup", e => { rpt = false; }, true);
        },
        mouse_unload() {
            this._on("touchend mouseup", this.mouse_up, true);
        },
        mouse_down(e) {
            if (e.repeat != undefined) rpt = e.repeat;
            if (rpt) return;
            if ([app.keyCodes[e.keyCode], "none"].indexOf(prefs.options.hotkeys.modifier) > -1)
                timer = new Date().getTime();
        },
        mouse_up(e) {
            this.selection = app.getSelection;
            if (this.selection.toString()) {
                if (new Date().getTime() - timer < 2000) {
                    this.container = this.selection.focusNode.parentElement;
                    if (
                        /\S/.test(this.selection.toString()) &&
                        this.selection.toString().length > 0
                    )
                        app.menu_move(e);
                }
            }
        },        
        get getSelection(){
            if (window.getSelection) {
                this.selection = window.getSelection();
                 return this.selection;
             } 
        },
        get position() {
            if(!this.selection.toString()) return false;

            var bb = this.selection.getRangeAt(0).getBoundingClientRect(), 
                clientWidth = document.documentElement.clientWidth;            

            bb = {  
                top: Math.max(bb.top,0) + window.pageYOffset,
                left : Math.max(bb.left,0) + window.pageXOffset,
                dim : {height: bb.height, width:bb.width}
            }

            if(bb.left > clientWidth) bb.left = clientWidth - 70

            return {
                style: bb,
                phrase: this.selection.toString()
            };
        },
        get isTranslator(){
            return (/googletranslate/.test(dictionary) || /deepl/i.test(lang)) && data.method === "define";
        },
        _translatePage(){
            if (prefs.options.extras.inlineTranslate === "all" && /googletranslate/.test(dictionary) && !gt.has(window.location.href)) {
                gt.add(window.location.href);
                let addScrpt = (arg, callback) => {
                    let s = document.createElement('script');
                    s.type = "text/javascript";
                    s.charset = "UTF-8";
                    s[arg] = callback;
                    document.getElementsByTagName('head')[0].appendChild(s);
                };
                addScrpt("text", "function googleTranslateElementInit(){ new google.translate.TranslateElement(); }", null)
                //addScrpt("src", "https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit&tl=&sl=&hl=")
            }
        },
        _normalize(dictionary){
            lang = (l => l.substring(0, l.indexOf("#") - 1).trim().toLocaleLowerCase())(prefs.dictionaries[dictionary][0]);
            lang = data.method === "tts" && dictionary === "reverso" ? "auto" : lang;
            return lang
        },      
        _hasClass(el, sel) {
            return el.classList.contains(sel);
        },
        _hide() {
            window.top.postMessage({ cmd: "panel__hide" }, "*");
        },
        _show(e) {
            app._hide();
            data.method = app._hasClass(e.target, "k-menu--dictionary")
                ? "define"
                : app._hasClass(e.target, "k-menu--speak")
                ? "tts"
                : null;

            if (!data.method) {
                app.menu_hide();
                app.console_create();
                return false;
            }

            Object.assign(data, app.position); 

            app._assign(data);

            window.top.postMessage(data, "*");

            app.menu_busyIndicator();
        },
        _assign(data) { 

            dictionary = prefs.dictionary[data.method];            

            lang = this._normalize(dictionary)

            if (this.isTranslator) {
                if (prefs.dictionary.default_translator !== dictionary) {
                    prefs.dictionary.default_translator = dictionary;
                    storage.set({ dictionary: prefs.dictionary });
                }
            }
            
            if(data.phrase.split(/\s/).length > 10 && !this.isTranslator && data.method === "define"){
                lang = prefs.dictionary.default_translator === "googletranslate" ? this._normalize("googletranslate") : "deepl";
                dictionary = prefs.dictionary.default_translator;
            }

            data.highlighter =
                prefs.options.extras.highlighter &&
                window.getSelection().rangeCount > 0;

            Object.assign(data,
                {
                    cmd: "panel__show",
                    type: "forge__url",
                    cors_active: prefs.cors.active,
                    predict: lang.indexOf("auto") > -1,
                    isMobile: app.isMobile,
                    dictionary: dictionary,
                    lang: lang,
                    rank: prefs.options.rank                
                }
            );
            data.inlineTranslate = prefs.options.extras.inlineTranslate && this.isTranslator ? prefs.options.extras.inlineTranslate : "";
            this._translatePage();
        },
        _updates(ps) {
            Object.assign(prefs, ps);
            this.menu_checkIcons();
            return;
        },
        console_create() {
            if (!console_element) {
                console_element = this.menu_create("blockquote", {
                        class: "k-console k-menu--easeIn"
                    }, document.body),
                    this.menu_create("span", {
                        class: "span-del"
                    }, console_element),
                    this.menu_create("textarea", {
                        class: "",
                        placeholder: chrome.i18n.getMessage("placeholder_console")
                    }, console_element),
                    this.menu_create("span", {
                        class: "span-ers"
                    }, console_element);

                this._getElements('.span-del', function(el) {
                    el.textContent = "✖";
                    el.addEventListener("click", app.console_remove)
                });
                this._getElements('.span-ers', function(el) {
                    el.addEventListener("click", function(e) {
                        console_element.querySelector("textarea").value = "";
                    })
                });
                this._getElements('textarea', function(el) {
                    el.addEventListener("keypress", app.console_keypress);
                    el.focus();
                });

                $q('.k-menu--console', menu_el).style.display = "none";
            }
        },
        console_computeStyle() {
            let alpha = document.documentElement.clientWidth < 768 ? 0.976 : 1.01;
            cstyle = `top:${(alpha * this.isMobile ? 77 : (parseFloat(window.getComputedStyle(console_element).top)-50)/
                                        document.documentElement.clientHeight*100).toFixed(1)}vh;
                      left:${(alpha*(parseFloat(window.getComputedStyle(console_element.querySelector("textarea")).width)+33)/
                                        document.documentElement.clientWidth*100).toFixed(1)}vw;
                       position:fixed;`;
            return cstyle;
        },
        console_keypress(e) {

            if (e.keyCode == 13 && !e.shiftKey) {
                e.preventDefault();

                if(window.getSelection().rangeCount) window.getSelection().removeAllRanges();

                let txt = e.target.value.replace(/[~$%^&*_|¯<\=>\\^]/gi, "").replace(/\r?\n|\r/g, "").trim();

                if (txt.startsWith("!")) {
                    let bs = txt.match(/^\!\w*\s*/i)[0].trim();
                    if (!bs || bs.length < 4) return;
                    app.console_hasBang(bs, e.target);
                    txt = txt.replace(bs, "").trim();
                }

                if (!txt) {
                    e.target.value = "";
                    return;
                }

                data.method = data.method || "define";
                data.inlineTranslate = false;
                data.highlighter = false;
                dictionary = prefs.dictionary[data.method];
                Object.assign(data,{
                        style: null,
                        phrase: txt,
                        method: data.method,
                        highlighter:false,
                        console: true
                    }
                );

                app._assign(data);
                menu_el.style = cstyle || app.console_computeStyle();
                menu_el.classList.add("k-menu--easeIn");
                app.menu_busyIndicator();
                window.setTimeout(app.menu_hide, app.isMobile ? 4000 : 3000);
                window.top.postMessage(data, "*");
            }
        },
        console_remove(e) {
            $q('.k-menu--console', menu_el).style.display = "block";
            $q("textarea", console_element).removeEventListener("keypress", app.console_keypress);
            console_element.remove();
            console_element = null;
        },
        console_hasBang(txt, textarea) {
            txt = txt.slice(1, txt.length);
            if (["def", "tts"].includes(txt.slice(0, 3))) {

                data.method = txt.slice(0, 3) == "def" ? "define" : "tts";

            } else {

                let r = new RegExp("^" + txt, "i"),
                    k = Object.keys(prefs.dictionaries),
                    dict = k.filter(t => r.test(t))[0];

                if (dict !== undefined && k.includes(dict)) {
                    data.method = ["ispeech", "responsivevoiceorg","watson", "webspeechapi"].includes(dict) ?
                        "tts" :
                        (dictionary = dict, "define");
                    prefs.dictionary[data.method] = dict;
                }
            }
            storage.set({ dictionary: prefs.dictionary });
            return;
        }
    }

    var app = new App(); 

