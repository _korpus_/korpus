"use strict";
var data = {},
    panel,
    tts, audio, 
    highlighter, 
    system_voices,
    timer_;


function TTS() {
    this._audio_container = null;
    this._button = null;
    this._onUtter = null;
    this.highlighter = null;
    this._opt = {
        voice: null,
        VOICES: system_voices,
        chunk: "",
        fw: 0,
        CHARLIMIT : 120
    };
    this.PARAGRAPHS = [];
    this.startOffset = 0;
}

TTS.prototype = { 
    _createElement(tag, attrs, parent) {
        if (!attrs) attrs = {};
        var elmt = document.createElement(tag);
        for (var i in attrs) {
            elmt.setAttribute(i, attrs[i]);
        }
        if (parent) parent.appendChild(elmt);
        return elmt;
    },

    _render(data, progressBar) {

        var b = document.body.querySelector(".k-menu--busyIndicator");
        if (b) b.classList.remove("k-menu--busyIndicator_active");

        this._audio_container = this._createElement(
            "div", {
                class: "k-audio--container",
                style: "opacity: 0.9;" + "top: 20px; left:20px;"
            },
            document.body
        );

        this._button = this._createElement(
            "span", {
                class: "k-button"
            },
            this._createElement(
                "div", {
                    class: "k-button--container"
                },
                this._audio_container
            )
        );

        this._setPosition(data.style);

        if (!this.webspeechapi) {
            audio = new Audio();
            this._button.parentElement.appendChild(audio);
        }
        
        this.progressbar = new progressBar(".k-loader_svg", this._button, {});
    },

    _setPosition(style) {
        this._button.parentElement.classList.add("k-button--container_transparent");
        if (style) {
            this._audio_container.style.left = `${Math.max(style.left - 45, 80)}px`;
            this._audio_container.style.top  = `${Math.max(style.top - 40, 80)}px`;
        } else {
            this._audio_container.style = cstyle;
        }
    },

    _error(e) {
        this._button.parentElement.style.backgroundColor = "transparent";
        this._button.parentElement.classList.remove("k-button--pulse");

        let loader = this._button.querySelector(".k-loader_svg");
        loader.nextElementSibling.style.display = "block";
        loader.setAttributeNS(null, "style", "fill:whitesmoke");
        loader.setAttributeNS(null, "stroke-width", "2");
        loader.setAttributeNS(null, "stroke", "rgb(3, 168, 124)");
        
        throw new Error(e);
    },

    _pulse(p) {
        if (p == undefined) return;
        if (this._audio_container) {
            if (this.webspeechapi)
                this._button.classList.toggle("k-button--pulse", p);
        }
        return;
    },

    _togglePlay(audio) {
        this._opt.is_playing = !this._opt.is_playing;
        this._pulse(this._opt.is_playing);
        audio[this._opt.is_playing ? (this.webspeechapi ? "resume" : "play") : "pause"]();
    },

    _chunkify(text, CHARLIMIT) {
        var punctuation = /[:!;?\(\)\[\]\u00a1\u00bf\u2014\u00ab\u201c\u201e\u2039\u300c\u300e\ufe41\ufe43]+/,
            dot_not_acronym = /[a-zA-Z\u00C0-\u017F](?:[a-zA-Z\u00C0-\u017F]\.)/,
            g = Math[((text.substr(2, text.length).search(punctuation) >= 0) && (text.search(dot_not_acronym) > -1))? "min" : "max"](text.substr(1,text.length).search(punctuation)+1, text.search(dot_not_acronym) + 2), 
            chunk = "";
        if (g >= CHARLIMIT || g <= 1) {
            if (g >= CHARLIMIT + 20 || g <= 1) {
                g = text.search(/,[^0-9]+/);
                if (g >= CHARLIMIT || g <= 1) {
                    let l = text.split(" ");
                    for (g = 0; g < l.length; g++) {
                        if (chunk.length + l[g].length + 1 > CHARLIMIT) {
                            l[g].length >= CHARLIMIT &&
                                (chunk += l[g].substr(0, CHARLIMIT - chunk.length - 1));
                            break;
                        }
                        chunk += (0 != g ? " " : "") + l[g];
                    }
                } else chunk = text.substr(0, g + 1)
            } else chunk = text.substr(0, g + 1); 
        } else chunk = text.substr(0, g + 1);

        text = text.substr(chunk.length, text.length - chunk.length).trim();
        return { chunk, text }
    },

    _matchCLDLanguageTag(d) {

        if (!d.predict) return this._opt.VOICES.get(d.lang);

        var l, w = null,
            voices = new Map();

        this._opt.VOICES.forEach(v => { // 
            l = v.lang.split(/-|_/)[0];
            if (!voices.get(l)) voices.set(l, []);
            voices.get(l).push(v);
        });

        d.lang = /pt-pt|pt-br|sr-me|sit-mp/.test(d.lang)
            ? d.lang.split(/-|_/)[0]
            : d.lang;

        if (d.lang.length == 3) {
            if (
                d.lang == "fil" &&
                navigator.appVersion.indexOf("Win") != -1 &&
                voices.keys().includes("fil")
            )
                w = voices.get(d.lang);

            if (d.lang == "syr" || d.lang == "sit") w = null;            
        } else {
            if (voices.get(d.lang)) {
                let prosodies = voices.get(d.lang).map(v => v.lang.split(/-|_/)[1]),
                    prosodies_all = {"af":["ZA"],"ar":["SA","AE","BH","DZ","EG","IQ","JO","KW","LB","LY","MA","OM","QA","SY","TN","YE"],"az":["AZ"],"be":["BY"],"bg":["BG"],"bs":["BA"],"ca":["ES"],"cs":["CZ"],"cy":["GB"],"da":["DK"],"de":["DE","AT","CH","LI","LU"],"dv":["MV"],"el":["GR"],"en":["GB","US","AU","scotland","BZ","CA","CB","IE","JM","NZ","PH","TT","ZA","ZW"],"es":["ES","MX","AR","BO","CL","CO","CR","DO","EC","GT","HN","NI","PA","PE","PR","PY","SV","UY","VE"],"et":["EE"],"eu":["ES"],"fa":["IR"],"fi":["FI"],"fo":["FO"],"fr":["FR","CA","BE","CH","LU","MC"],"gl":["ES"],"gu":["IN"],"he":["IL"],"hi":["IN"],"hr":["BA","HR"],"hu":["HU"],"hy":["AM"],"id":["ID"],"is":["IS"],"it":["IT","CH"],"ja":["JP"],"ka":["GE"],"kk":["KZ"],"kn":["IN"],"ko":["KR"],"ky":["KG"],"lt":["LT"],"lv":["LV"],"mi":["NZ"],"mk":["MK"],"mn":["MN"],"mr":["IN"],"ms":["BN","MY"],"mt":["MT"],"nb":["NO"],"nl":["NL","BE"],"ns":["ZA"],"pa":["IN"],"pl":["PL"],"ps":["AR"],"pt":["PT","BR"],"qu":["BO","EC","PE"],"ro":["RO"],"ru":["RU"],"sa":["IN"],"se":["SE","NO","FI"],"sk":["SK"],"sl":["SI"],"sq":["AL"],"sr":["BA","SP"],"sv":["SE","FI"],"sw":["KE"],"syr":["SY"],"ta":["IN"],"te":["IN"],"th":["TH"],"tl":["PH"],"tn":["ZA"],"tr":["TR"],"tt":["RU"],"uk":["UA"],"ur":["PK"],"uz":["UZ"],"vi":["VN"],"xh":["ZA"],"zh":["CN","CN_#Hans","HK","HK_#Hans","HK_#Hant","TW","TW_#Hant","MO","SG"],"zu":["ZA"]};

                for (l of prosodies_all[d.lang])
                    if (prosodies.indexOf(l) > -1) {
                        w = voices.get(d.lang).filter(v => v.lang.split(/-|_/)[1] == l);
                        w = w[0];
                        break;
                    }
            }
        }

        if(!w){
            this._error("NO_VOICE");
            return;
        }

        return w;
    }, 

    _clear(selection){
        selection = selection || window.getSelection();
        if (selection.rangeCount > 0)
            selection.empty
                ? selection.empty()
                : selection.removeAllRanges && selection.removeAllRanges();
    },

    _highlight(tts){
        if (!data.highlighter) return;
        this._clear();
        document.body.classList.add("k-light");
    },    

    getWordAt(str, pos) {
        str = String(str);
        pos = Number(pos) >>> 0;

        if(/\s+/.test(str.slice(0, pos + 1))) ++pos;
        
        var left = str.slice(0, pos + 1).search(/\S+$/),
            right = str.slice(pos).search(/\s/);

        if (right < 0) {
            return str.slice(left);
        }

        return str.slice(left, right + pos);
    },

    _next(paragraph, settings, callback) {
        if (paragraph) {
            if (this._audio_container) {
                settings.part_index++;
                this._narrate(settings, callback);
            }
        } else {
            if (callback !== undefined) {
                callback();
                settings.offset = undefined;
                this._opt.is_playing = false;
                if (this._opt.audio_parts.size > 0 && !this.webspeechapi)  
                    this._opt.audio_parts_completed = true;

                this.progressbar.completed = true;
                this.progressbar.opt.duration = this.progressbar.duration;
                this.progressbar.opt.isPaused = true;
                this.progressbar.stroke_clear();
                
                this._narrate(settings, callback);
            }
        }
    },

    _narrate(settings, callback) { 

        if (++this._opt.fw > 50) return; 
        
        var self = this,
            startOffset, utterance, paragraph;
        settings = settings || {};

        paragraph = this.PARAGRAPHS[settings.part_index];

        if(paragraph){
            if (this.webspeechapi) window.speechSynthesis.cancel();
            if (data.highlighter) this.highlighter = new Highlighter(paragraph.element);
            this._opt.chunk =  paragraph.chunk;      
        } else {
            // no more chunk
            if (!this.webspeechapi) {
                this.progressbar.duration = Array.from(self._opt.audio_parts).map(a => a[1].duration).reduce((i, j) => i + j);
                this.progressbar.duration = Math.round(self.progressbar.duration * 10) / 10;
            }
            settings.part_index = 0;
            this._next(paragraph, settings, callback)
            return;
        }

        this._onUtter = {
            handleEvent(e) {
                 switch (true) {
                    case /start|play|end|ended/.test(e.type):
                        self._pulse(/start|play/.test(e.type)); 
                        utterance.removeEventListener(e.type, self._onUtter);                                           
                        if (data.highlighter && /start|play/.test(e.type)) {
                            self.webspeechapi
                                ? paragraph.element.classList.add("k-narrating")
                                : self.highlighter.highlightPhrase(self.PARAGRAPHS, settings.part_index, self._opt.audio_parts_completed);

                            let bb = paragraph.element.getBoundingClientRect();
                            if (bb.top < 0 || bb.bottom > window.innerHeight) {
                                paragraph.element.scrollIntoView({ behavior: "smooth", block: "nearest"});
                            }
                        }
                        if (/end|ended/.test(e.type)) {
                            if (!window) return;
                            if (data.highlighter) {
                                self.highlighter && self.highlighter.remove();
                                self.webspeechapi && paragraph.element.classList.remove("k-narrating");
                            }
                            if (!self.webspeechapi)
                                if (!self.progressbar.completed)
                                    self.progressbar.stroke_clear();

                            self._next(paragraph, settings, callback);
                        }
                        break;
                    case /boundary/.test(e.type):
                        if (e.name != "word") return;
                        let word = self.getWordAt(self._opt.chunk, e.charIndex);
                        self.highlighter.highlightWord(paragraph.startOffset + e.charIndex, word.length);
                        break;
                    case /error|pause|resume/.test(e.type):
                        self._pulse(!/error|pause/.test(e.type) );
                        break;
                }
            }
        };

        utterance =
            this._opt.audio_parts_completed 
                ? this._opt.audio_parts.get(settings.part_index)
                : this._config(this._opt.chunk, settings.part_index); 

        ["error", "start play", "end ended"].forEach(evt => {
            evt = evt.split(/\s+/);
            evt = evt[this.webspeechapi ? 0 : 1] || evt[0];
            utterance.addEventListener(evt, this._onUtter);
        });

        "pause resume".split(/\s+/).forEach(evt => utterance.addEventListener(evt, this._onUtter));

        let isIndexNull = index => {
            if (!index) {
                if (data.highlighter)
                    self.highlighter.startOffset = self.PARAGRAPHS[index].startOffset;
                audio.pause();
            }
        }

        if (this.webspeechapi) {
            utterance.addEventListener("boundary", this._onUtter)
            setTimeout(() => {
                audio.speak(utterance);
            }, 0);
            isIndexNull(settings.part_index);
        } else {
            audio = utterance;
            audio.play()
                .then(() => isIndexNull(settings.part_index))
                .catch(() => self.pulse(false));
        }
    },

    _config(text, index) {
        let a;
        if (this.webspeechapi) {
            if(!this._opt.voice) return;
            audio = window.speechSynthesis;
            a = new window.SpeechSynthesisUtterance(text);
            a.voice = this._opt.voice;
            a.rate = 1;
            a.pitch = 1;
            this._opt.audio_parts.set(index, a);
        } else {
            let A = function(text, index) {
                let a = new Audio();
                a.playbackRate = 1;
                a.preload = "auto";
                a.controls = false;
                a.style.display = "none";
                a.src = data.url.replace("{{text}}", text);
                return a;
            };

            if (index) {
                a = this._opt.audio_parts.get(index);
                this.progressbar._stroke_start({
                    isPaused: false,
                    duration: a.duration - a.currentTime
                });
            } else {
                a = A(text, index);
                if (!a.canPlayType) {
                    this._error("AUDIO_NOT_SUPPORTED")
                    return;
                }
                this._opt.audio_parts.set(index, a);
            }

            if (this.PARAGRAPHS[index + 1]) {
                let a = A(this.PARAGRAPHS[index + 1].chunk, index + 1)
                this._opt.audio_parts.set(index + 1, a);
            }
        }        
        return a
    },

    async _start(data) {
        this.PARAGRAPHS = []; 
        if(!data.console)
            if (this.webspeechapi) {
                paragraphs = await utils.paragraphs;
                paragraphs.forEach(p => {
                    p.chunk = p.element.textContent.slice(p.startOffset, p.endOffset).replace(/(\d+)\s(\d{2,})/g, "$1$2");
                })
                this.PARAGRAPHS = paragraphs;
                this._opt.CHARLIMIT = 5000;
            } else {
                if(data.text.length > 1000) return
                paragraphs = await init(utils, wrapper, false);
                text_norm = wrapper.nodeToText(paragraphs, " ");
                data.text = text_norm.replace(/\n/g, "");
                for (let [k, v] of paragraphs) {
                    let p, c, t = v.element.textContent.slice(v.startOffset, v.endOffset), i = 0;
                    while (t && t.length > 2 && i < 50) {
                        c = this._chunkify(t, 120);
                        p = Object.assign({},v); 
                        p.chunk = c.chunk;
                        p.startOffset = i ? this.PARAGRAPHS[i - 1].endOffset + 1 : v.startOffset;
                        p.endOffset = p.startOffset + p.chunk.length;
                        this.PARAGRAPHS.push(p);
                        t = c.text, i++;
                    }
                }            
            }
        else this.PARAGRAPHS.push({ chunk: data.text, startOffset: 0 })

        this._highlight(tts);

        this._narrate({
            CHARLIMIT: this._opt.CHARLIMIT,
            part_index: 0
        }, function() {
            console.log('end');
        })
    },

    _init(data) {
        data.text = data.phrase;
        this.webspeechapi = data.dictionary == "webspeechapi";

        Object.assign(this._opt, {
            audio_parts_completed: false,
            audio_parts: new Map(),
            is_playing: false,
            chunk: "",
            fw: 0
        });

        this._render(data, progressBar);
        document.body.addEventListener("click", this);

        if (this.webspeechapi) {
            if (!'speechSynthesis' in window) {
                this._error("NO_SPEECH_SYNTHESIS");
                return;
            }
            this._opt.voice = this._matchCLDLanguageTag(data);
        }
    },

    _unload() {
        var self = this;
        self._audio_container.style.opacity = 0;

        setTimeout(function() {
            self._audio_container.remove();
            self._audio_container = null;
            if (self.webspeechapi) {
                window.speechSynthesis.cancel();
                if (self.highlighter) {
                    self.highlighter.remove();
                    self.highlighter = null;
                    document.body.classList.toggle("k-light");
                }
                self._onUtter = null;
                self._opt.is_playing = false;
            } else { 
                audio.setAttribute("src", "");
                audio = null;
            }
            if (paragraphs) wrapper.original(paragraphs);
            document.body.removeEventListener("click", self);
        }, 500);
        },

    handleEvent(e) {
        if (this._audio_container) {
            if (
                e.target == this._audio_container ||
                e.target == this._button ||
                e.target == this._button.querySelector(".k-loader_svg")
            ) {
                this._togglePlay(audio);
                if (!this.webspeechapi)
                    if (audio.networkState === audio.NETWORK_NO_SOURCE) this._error("NETWORK_NO_SOURCE");

            } else this._unload();
        }
    },

    speak(data) {
        if (!this._audio_container) this._init(data);
        this._start(data).then().catch(e => {
            this._error("START");
            console.log(e);
        });
    }
};

function progressBar(svg, parent, opt) {
    this.path = this._render(svg, parent);
    this.opt = Object.assign(opt, {
        pathLength: this.path.getTotalLength(),
        offset: 0,
        timer: 0,
        isPaused: true
    });
    this.duration = 0;
    this.completed = false;
    this.webspeechapi = data.dictionary == "webspeechapi";
    this.path.addEventListener("click", this);
}

progressBar.prototype = {
    _createSVG(NS, attrs, parent) {
        if (!attrs) attrs = {};
        var el = document.createElementNS("http://www.w3.org/2000/svg", NS);
        for (var i in attrs) {
            el.setAttributeNS(null, i, attrs[i]);
        }
        if (parent) parent.appendChild(el);
        return el;
    },

    _render(cls, parent) {
        var g = this._createSVG(
                "g", { transform: "translate(-1,-1)" },
                this._createSVG(
                    "svg", {
                        viewBox: "0 0 40 40",
                        preserveAspectRatio: "xMidYMid meet"
                    },
                    parent
                )
            ),
            path = this._createSVG(
                "path", {
                    class: cls.replace(".", ""),
                    d: "M21 3C11.059 3 3 11.059 3 21s8.059 18 18 18 18-8.059 18-18S30.941 3 21 3z"
                },
                g
            ),
            error = this._createSVG(
                "path", {
                    d: "M10 21h25",
                    class: "k-button_svg--error",
                    stroke: "#333",
                    "stroke-width": "2",
                    "stroke-linecap": "square",
                    "stroke-linejoin": "bevel",
                    "stroke-dasharray": "2.46799994,7.40399981"
                },
                g
            );


        if (!this.webspeechapi) path.style.fill = "#03a87c" //"#446688";
        return path;
    },

    _stroke_style(offset) {
        this.path.style.strokeWidth = `${1 + 3 * offset / parseInt(this.opt.pathLength)}px`;
        this.path.style.strokeDashoffset = `${this.opt.pathLength + offset}px`;
    },

    _stroke_init() {
        if (this.opt.offset) return;
        let length = this.path.getTotalLength();
        this.path.style.stroke = "#03a87c";
        this.path.style.fill = "whitesmoke";
        this.path.style.strokeDasharray = parseFloat(length.toFixed(3), 10);
        this.path.style.strokeDashoffset = parseFloat(length.toFixed(3), 10);
        if (this.webspeechapi) {
            this.path.style.fill = "#03a87c";
            this.path.style.strokeWidth = "4px";
            this.path.style.strokeDashoffset = 0;
        }
    },

    _stroke_start(attrs) {
        this._stroke_init();
        
        if (this.webspeechapi) return;
        
        Object.assign(this.opt, attrs);     
        
        let fpsInterval = 1000 * this.opt.duration / this.opt.pathLength,
            delta = this.opt.pathLength / 80;
        this._setInterval(function() {
            this._stroke_increase(delta);
        }.bind(this), Math.round(delta * fpsInterval * 10) / 10, 80 - this.opt.timer)
    },

    _stroke_increase(delta) {
        if (audio) {
            if (this.opt.offset >= this.opt.pathLength || this.opt.timer > 80 || this.opt.isPaused) {
                if (this.opt.offset > this.opt.pathLength) this.stroke_clear();
                clearTimeout(timer_);
                return;
            } else {
                this.opt.offset += delta;
                this._stroke_style(this.opt.offset);
                ++this.opt.timer;
            }
        }
    },

    stroke_clear() {
        this.path.style.strokeDashoffset = 2 * this.opt.pathLength;
        this.opt.offset = 0;
        this.opt.timer = 0;
    },

    _setInterval(func, wait, times) {
        let interv = function(w, t) {
            return function() {
                if (typeof t === "undefined" || t-- > 0) {
                    timer_ = setTimeout(interv, w);
                    try {
                        requestAnimationFrame(function() {
                            func.call(null);
                        })                        
                    } catch (e) {
                        t = 0;
                        throw e.toString();
                    }
                }
            };
        }(wait, times);

        setTimeout(interv, wait);
    },

    handleEvent(e) {
        switch (e.type) {
            case "click":
                e.preventDefault();
                this.opt.isPaused = !this.opt.isPaused;
                this._stroke_start({
                    isPaused: this.opt.isPaused,
                    duration: this.completed ? this.duration : audio.duration
                });
                break;
        }
    }
}
 
function Highlighter(container) {
    var self = this;
    this.container = container || null;
    this.startOffset = 0;
    this.kTextStylesRules = ["font-family", "font-kerning", "font-size",
        "font-size-adjust", "font-stretch", "font-variant", "font-weight",
        "line-height", "letter-spacing", "text-orientation",
        "text-transform", "word-spacing"
    ];
    this.DEFAULT_LEAF_BLACKLIST_SELECTOR = "SUP, SUB, SCRIPT, STYLE, IMG, SVG".split(/,\s+/);
    this.selectorFilter = {
        acceptNode: function(node) {
            if (self.DEFAULT_LEAF_BLACKLIST_SELECTOR.indexOf(node.parentNode.nodeName) === -1) {
                if (/\S/.test(node.nodeValue)) 
                    return NodeFilter.FILTER_ACCEPT;
            }
        }
    };
}

Highlighter.prototype = {
    highlightWord(startOffset, length) {
        /* Prototype based on Mozilla Reader highlighter */
        let containerRect = this.container.getBoundingClientRect(),
            range         = this._getRange(startOffset, startOffset + length),
            rangeRects    = range.getClientRects(),
            computedStyle = window.getComputedStyle(range.endContainer.parentNode),
            nodes         = this._getFreshHighlightNodes(rangeRects.length);

        let style = {};
        for (let r of this.kTextStylesRules) 
            style[r] = computedStyle[r];        

        for (let i = 0; i < rangeRects.length; i++) {
            let r = rangeRects[i];
            let node = nodes[i];

            style = Object.assign({
                "top"    : `${r.top - containerRect.top + 4*r.height / 5}px`,
                "left"   : `${r.left - containerRect.left + r.width / 2}px`,
                "width"  : `${r.width}px`,
                "height" : `${r.height}px`,
            }, style);

            node.classList.toggle("k-newline", style.top != node.dataset.top);
            node.dataset.top = style.top;

            node.dataset.word = range.toString();

            node.style = Object.entries(style).map(s => `${s[0]}: ${s[1]};`).join(" ");}
    },

    highlightPhrase(p, i, c) {

        let STYLE = {},
        computedStyle = window.getComputedStyle(p[i].element);

        for (let r of this.kTextStylesRules) 
            STYLE[r] = computedStyle[r];  

        this._getFreshHighlightNodesPhrase(p, i, c);

        p[i].element.classList.add("k-narrating-phrase");

        let range = document.createRange(),
        _ = p[i];

        _.element.textContent = _.element.textContent;
        range.setStart(_.element.firstChild, _.startOffset);
        range.setEnd(_.element.firstChild, Math.min(_.endOffset, _.element.firstChild.textContent.length));

        let containerRect = _.element.getBoundingClientRect(),
            rangeRect = range.getClientRects();

        for (var r of rangeRect) {
            let style = Object.assign({
                "top"    : `${r.top - containerRect.top}px`,
                "left"   : `${r.left - containerRect.left}px`,
                "width"  : `${r.width}px`,
                "height" : `${r.height}px`
            }, STYLE);

            var div = document.createElement("div");
            div.classList.add("k-narrate-phrase-highlight");
            _.element.appendChild(div);
            div.style = Object.entries(style).map(
                s => `${s[0]}: ${s[1]};`).join(" ");
        }
    },

    _getFreshHighlightNodes(count) {
        let doc = this.container.ownerDocument;
        let nodes = Array.from(this._nodes);

        for (let toRemove = 0; toRemove < nodes.length - count; toRemove++) {
            nodes.shift().remove();
        }

        for (let toAdd = 0; toAdd < count - nodes.length; toAdd++) {
            let node = doc.createElement("div");
            node.className = "k-narrate-word-highlight";
            this.container.appendChild(node);
            nodes.push(node);
        }

        return nodes;
    },

    _getFreshHighlightNodesPhrase(p, i, c) {
        let nodes = index => {
            (c || index != i) && p[index].element.classList.remove("k-narrating-phrase");
            return Array.from(
                p[index].element.querySelectorAll(".k-narrate-phrase-highlight")
            );
        };
        nodes =
            i && p[i].element != p[i - 1].element
                ? nodes(i - 1)
                : c
                ? nodes(p.length - 1)
                : nodes(i);
        if (nodes.length > 0) for (var node of nodes) node.remove();
    },

    _getRange(startOffset, endOffset) {
        let doc = this.container.ownerDocument; 
        let i = 0;
        let walker = doc.createTreeWalker(
            this.container, doc.defaultView.NodeFilter.SHOW_TEXT, this.selectorFilter, false);
        let node = walker.nextNode();

        function _findNodeAndOffset(offset) {
            do {
                let length = node.data.length;
                if (offset >= i && offset <= i + length) {
                    return [node, offset - i];
                }
                i += length;
            } while ((node = walker.nextNode()));

            node = walker.lastChild();
            return [node, node.data.length];
        }

        let range = doc.createRange();
        range.setStart(..._findNodeAndOffset(startOffset));
        range.setEnd(..._findNodeAndOffset(endOffset));

        return range;
    },

    get _nodes() {
        return this.container.querySelectorAll(".k-narrate-word-highlight");
    },

    remove() {
        for (let node of this._nodes) {
            node.remove();
        }
        this.container.classList.remove("k-narrating");
        this.container = null;
    }
}

function Korpus(opt) {

    this.data      = data;
    this.opt       = Object.assign({}, opt);
    this.iframe    = null;
    this.container = null;
    this.dragItem  = null;
    this.dragStart = null;
    this.dragEnd   = null;
    this.drag      = null;

    chrome.runtime.onMessage.addListener((msg, sender, response) => { this._show(msg); });

    if (!system_voices) this.getVoices().then(voices => {
        system_voices = new Map();
        voices.forEach(v => system_voices.set(v.lang.toLocaleLowerCase(), v))
    });

    window.addEventListener(
        "message",
        function(e) { if (e.data) this._onMessage(e); }.bind(this),
        false
    );

    storage.get("options", r => this._resize(r));
}

Korpus.prototype = {
    _hasClass(sel, el, iframe) {
        el = this.iframe.parentElement || el;
        return el.classList.contains(sel);
    },

    _load() {
        this.dragItem = document.createElement("div");
        this.dragItem.className = "k-drag";
        this.iframe = document.createElement("iframe");
        this.iframe.className = "k-panel";
        this.iframe.name = "k_panel";
        this.iframe.style = (s => (`width: ${s.width}px; height: ${s.height}px;`))(this.opt);
        this.iframe.src = "about:blank";
        this.container = document.createElement("div");
        this.container.className = "k-container";
        this.container.appendChild(this.dragItem);
        this.container.appendChild(this.iframe);
        document.body.appendChild(this.container);

        this._draggable(this.dragItem, this.container, "add");
        if (!(/googletranslate/.test(data.dictionary) || /iciba|deepl/.test(data.lang)))
            this.iframe.sandbox = "allow-same-origin";

        document.body.addEventListener("click", this);
    },

    _unload() {
        this._hide();
        
        if (this.drag) this._draggable(this.dragItem, this.container, "remove");

        this.iframe.remove();
        this.container.remove();
        this.iframe    = "",
        this.container = "",
        this.drag      = "",
        this.dragStart = "",
        this.dragEnd   = "";

        if(paragraphs) wrapper.original(paragraphs);
    },

    _show(msg) {
        if(data.inlineTranslate === "all") return;
        if (msg.type == "forge__url") {
            data = msg;
            data.url = (d => d.url.replace(/{{phrase}}/, d.phrase).replace(/{{lang}}/g, d.lang))(data);

            if (data.method == "define") {
                if (!this.iframe) this._load();

                if (data.cmd == "panel__show") {
                    this._setPosition(data.style, this.container);
                }

            if (/^http:/.test(data.url.split("/")[0]))
                this._http_over_https(data, "cors-anywhere.herokuapp.com");

                this.iframe.src = data.url;
            } else {
                if (!tts) tts = new TTS();
                tts.speak(data);
            }
        } else if (msg.type == "DOM__content__cleaned") {
            if(!data.inlineTranslate) 
                this.iframe.parentElement.classList.add("k-panel--visible");
            setTimeout(() => this._clear(), data.inlineTranslate ? 2000 : 0);        
        }
    },

    _clear(selection){
        selection = selection || window.getSelection();
        if (selection.rangeCount > 0)
            selection.empty
                ? selection.empty()
                : selection.removeAllRanges && selection.removeAllRanges();
    },

    _hide() {
        if (this.iframe) 
            this.iframe.parentElement.classList.remove("k-panel--visible");        
    },

    _http_over_https(data, cors_api_host) {
        if (data.cors_active) {
            if (/^https:/.test(window.location.protocol) &&
                /^http:/.test(data.url.split("/")[0]) &&
                data.method == "define") {
                data.url = "https://" + cors_api_host + "/" + data.url;
                storage.get("cors", function(r) {
                    r.cors.Origin = `${window.location.protocol + "//" + window.location.host}`;
                    storage.set({
                        cors: r.cors
                    });
                });
                /*chrome.runtime.sendMessage({
                    type: "enable__badge__cors"
                });*/
            }
            /*else {
                               chrome.runtime.sendMessage({
                                   type: "disable__badge__cors"
                               });
                           }*/
        }
    },

    _resize(r) {
        ["height", "width"].forEach(s => {
            this.opt[s] = parseInt(r.options.extras[s]) || this.opt[s];
        });
        this.opt.width =
            this.opt.width > window.innerWidth
                ? 0.95 * window.innerWidth
                : this.opt.width;
    },

    _setPosition(bb, el) {
        let si = bb ? "px" : "%";
        el.style.setProperty("--position", bb ? "absolute" : "fixed");

        bb = bb
            ? this._isPanelInView(bb, this.opt.width, -60, this.opt.height, 20)
            : { top: 27, left: 5 };

        el.style.setProperty("--left", parseInt(bb.left) + si);
        el.style.setProperty("--top", parseInt(bb.top) + si);
        if (el.style.transform) el.style.removeProperty("transform");
    },

    _isPanelInView(bb, w, dw, h, dh) {
        let top = bb.top + bb.dim.height + dh;
        top = top + h > document.body.clientHeight ? (window.pageYOffset + window.innerHeight - h - dh) : top;
        top = Math.max(top, 20);

        let left = bb.left + bb.dim.width/3 + dw;
        left = left + w > window.innerWidth ? window.innerWidth - w + dw : left;
        left = Math.max(left, 20);
        return { top, left };
    },

    _draggable(dragItem, container, action) {
        var active = false,
            X, Y, X0, Y0, dX = 0,
            dY = 0,
            touch = e => ["touchstart", "touchmove"].indexOf(e.type) > -1 ? e.touches[0] : e,
            delta = (e, X, d) => touch(e)[X] - d;

        function setTranslate(xPos, yPos, el) {
            el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
        }

        if (!this.drag) {
            this.dragStart = function(e) {
                X0 = delta(e, "clientX", dX);
                Y0 = delta(e, "clientY", dY);
                active = e.target === dragItem;
            };
            this.drag = function(e) {
                e.preventDefault();
                if (active) {
                    X = delta(e, "clientX", X0);
                    Y = delta(e, "clientY", Y0);
                    dX = X;
                    dY = Y;
                    setTranslate(X, Y, container);
                }
            };
            this.dragEnd = function(e) {
                X0 = X;
                Y0 = Y;
                active = false;
            };
        };

        "touchstart mousedown touchmove mousemove touchend mouseup".split(/\s+/).forEach(e => {
            document.body[action + "EventListener"](e, this);
        })
    },

    handleEvent(e) {
        switch (true) {
            case /touchstart|mousedown/.test(e.type):
                this.dragStart(e)
                break;
            case /touchmove|mousemove/.test(e.type):
                this.drag(e)
                break;
            case /touchend|mouseup/.test(e.type):
                this.dragEnd(e)
                break;
            case /click/.test(e.type):
                if (!(e.target == this.iframe || e.target == this.iframe.previousElementSibling && parent.document)) {
                    this._unload();
                    document.body.removeEventListener("click", this);
                }
                break;
        }
    },

    _onMessage(e) {
        let cmd = e.data.cmd;
        if (cmd == "panel__hide" || cmd == "hash__changed") {
            this._hide();
        } else if (
            (e.source == e.target && cmd == "panel__show") ||
            cmd == "search__phrase" ||
            cmd == "search__phrase__dict"
        ) {
            this._sendMessage(e.data, cmd);
        } else if (cmd == "DOM__content__loaded") {
            e.ports[0].postMessage(data);
        } else if (cmd == "DOM__content__cleaned") {
            this._show({ type: cmd });
            
        } 

        if (cmd && cmd.startsWith("panel__")) {
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    },

    _sendMessage(data, cmd) {
        if (cmd == "search__phrase__dict") {
            lang = (l => l
                    .substring(0, l.indexOf("#") - 1)
                    .trim()
                    .toLocaleLowerCase())(prefs.dictionaries[data.dictionary][0]);

            lang = data.method == "tts" && data.dictionary == "reverso" ? "auto" : lang;
            data.type = "forge__url";
            data.lang = lang;
        }
        chrome.runtime.sendMessage(data);
    },

    getVoices() {
        return new Promise(r => {
            var v = window.speechSynthesis.getVoices();
            if (v.length) r(v);
            else
                window.speechSynthesis.onvoiceschanged = () => {
                    v = window.speechSynthesis.getVoices();
                    r(v);
                };
        });
    }
};

new Korpus({
    height: 250,
    width: 450
});