"use strict";

var ISO_639_1_LANGCODE = {"aa":"Afar","ab":"Abkhazian","ae":"Avestan","af":"Afrikaans","ak":"Akan","am":"Amharic","an":"Aragonese","ar":"Arabic","as":"Assamese","av":"Avaric","ay":"Aymara","az":"Azerbaijani","ba":"Bashkir","be":"Belarusian","bg":"Bulgarian","bh":"Bihari","bi":"Bislama","bm":"Bambara","bn":"Bengali (Bangla)","bo":"Tibetan","br":"Breton","bs":"Bosnian","ca":"Catalan","ce":"Chechen","ch":"Chamorro","co":"Corsican","cr":"Cree","cs":"Czech","cu":"Old Church Slavonic,ld Bulgarian","cv":"Chuvash","cy":"Welsh","da":"Danish","de":"German","dv":"Divehi,hivehi,aldivian","dz":"Dzongkha","ee":"Ewe","el":"Greek","en":"English","eo":"Esperanto","es":"Spanish","et":"Estonian","eu":"Basque","fa":"Persian (Farsi)","ff":"Fula,ulah,ulaar,ular","fi":"Finnish","fil":"Filipino","fj":"Fijian","fo":"Faroese","fr":"French","fy":"Western Frisian","ga":"Irish","gd":"Gaelic (Scottish)","gl":"Galician","gn":"Guarani","gu":"Gujarati","gv":"Gaelic (Manx)","ha":"Hausa","he":"Hebrew","hi":"Hindi","ho":"Hiri Motu","hr":"Croatian","ht":"Haitian Creole","hu":"Hungarian","hy":"Armenian","hz":"Herero","id":"Indonesian","ie":"Interlingue","ig":"Igbo","ii":"Sichuan Yi; nuosu","ik":"Inupiak","io":"Ido","is":"Icelandic","it":"Italian","iu":"Inuktitut","ja":"Japanese","ji":"Yiddish","jv":"Javanese","ka":"Georgian","kg":"Kongo","ki":"Kikuyu","kj":"Kwanyama","kk":"Kazakh","kl":"Kalaallisut,reenlandic","km":"Khmer","kn":"Kannada","ko":"Korean","kr":"Kanuri","ks":"Kashmiri","ku":"Kurdish","kv":"Komi","kw":"Cornish","ky":"Kyrgyz","la":"Latin","lb":"Luxembourgish","lg":"Luganda,anda","li":"Limburgish ( Limburger)","ln":"Lingala","lo":"Lao","lt":"Lithuanian","lu":"Luga-Katanga","lv":"Latvian (Lettish)","mg":"Malagasy","mh":"Marshallese","mi":"Maori","mk":"Macedonian","ml":"Malayalam","mn":"Mongolian","mo":"Moldavian","mr":"Marathi","ms":"Malay","mt":"Maltese","my":"Burmese","na":"Nauru","nb":"Norwegian bokmål","nd":"Northern Ndebele","ne":"Nepali","ng":"Ndonga","nl":"Dutch","nn":"Norwegian nynorsk","no":"Norwegian","nr":"Southern Ndebele","nv":"Navajo","ny":"Chichewa,hewa,yanja","oc":"Occitan","oj":"Ojibwe","om":"Oromo (Afaan Oromo)","or":"Oriya","os":"Ossetian","pa":"Punjabi (Eastern)","pi":"Pāli","pl":"Polish","ps":"Pashto,ushto","pt":"Portuguese","qu":"Quechua","rm":"Romansh","rn":"Kirundi","ro":"Romanian","ru":"Russian","rw":"Kinyarwanda (Rwanda)","sa":"Sanskrit","sd":"Sindhi","se":"Sami","sg":"Sango","sh":"Serbo-Croatian","si":"Sinhalese","sk":"Slovak","sl":"Slovenian","sm":"Samoan","sn":"Shona","so":"Somali","sq":"Albanian","sr":"Serbian","sr-me":"Montenegrin","ss":"Siswati,Swati","st":"Sesotho","su":"Sundanese","sv":"Swedish","sw":"Swahili (Kiswahili)","ta":"Tamil","te":"Telugu","tg":"Tajik","th":"Thai","ti":"Tigrinya","tk":"Turkmen","tl":"Tagalog","tn":"Setswana","to":"Tonga","tr":"Turkish","ts":"Tsonga","tt":"Tatar","tw":"Twi","ty":"Tahitian","ug":"Uyghur","uk":"Ukrainian","ur":"Urdu","uz":"Uzbek","ve":"Venda","vi":"Vietnamese","vo":"Volapük","wa":"Wallon","wo":"Wolof","xh":"Xhosa","yo":"Yoruba","za":"Zhuang,huang","zh":"Chinese","zh-hans":"Chinese (Simplified)","zh-hant":"Chinese (Traditional)","zu":"Zulu"},
string                 = {},
forge                  = {},
lang_guess,
guess, 
dictionary, lang, url, word, phrase;

string.i18nDetectLanguage = function(txt) {
    return new Promise(function(r, s) {
        txt = string.chunkify(txt, 100),
        chrome.i18n.detectLanguage(txt, n => r(n ? n.languages[0] : {}))
    })
};

string.richtrDetectLanguage = function(txt) {
    if (!guess) guess = new guessLanguage();
    lang_guess = {};
    return new Promise(function(r, s) {
        guess.detect(txt, function(l) {
            lang_guess.language = l, r(l)
        })
    })
};

string.base64 = function() {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    this.encode = function(r) { var t, e, o, a, h, n, C, i = "", c = 0; for (r = string.base64.utf8Encode(r); c < r.length;) t = r.charCodeAt(c++), e = r.charCodeAt(c++), o = r.charCodeAt(c++), a = t >> 2, h = (3 & t) << 4 | e >> 4, n = (15 & e) << 2 | o >> 6, C = 63 & o, isNaN(e) ? n = C = 64 : isNaN(o) && (C = 64), i = i + _keyStr.charAt(a) + _keyStr.charAt(h) + _keyStr.charAt(n) + _keyStr.charAt(C); return i };
    this.utf8Encode =function(r) { r = r.replace(/\r\n/g, "\n"); for (var t = "", e = 0; e < r.length; e++) { var o = r.charCodeAt(e); 128 > o ? t += String.fromCharCode(o) : o > 127 && 2048 > o ? (t += String.fromCharCode(o >> 6 | 192), t += String.fromCharCode(63 & o | 128)) : (t += String.fromCharCode(o >> 12 | 224), t += String.fromCharCode(o >> 6 & 63 | 128), t += String.fromCharCode(63 & o | 128)) } return t };
    this.utf8Decode= function(r) { const e = r.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g, function(r) { var e = (15 & r.charCodeAt(0)) << 12 | (63 & r.charCodeAt(1)) << 6 | 63 & r.charCodeAt(2); return String.fromCharCode(e) }).replace(/[\u00c0-\u00df][\u0080-\u00bf]/g, function(r) { var e = (31 & r.charCodeAt(0)) << 6 | 63 & r.charCodeAt(1); return String.fromCharCode(e) }); return e };
    this.decode=function(r) { for (var t, A, E = "", C = string.base64.decodeAsBytes(r), a = C.length, o = 0, e = 0; a > o;) if (t = C[o++], 127 >= t) E += String.fromCharCode(t); else { if (t > 191 && 223 >= t) A = 31 & t, e = 1; else if (239 >= t) A = 15 & t, e = 2; else { if (!(247 >= t)) throw "not a UTF-8 string"; A = 7 & t, e = 3 } for (var h = 0; e > h; ++h) { if (t = C[o++], 128 > t || t > 191) throw "not a UTF-8 string"; A <<= 6, A += 63 & t } if (A >= 55296 && 57343 >= A) throw "not a UTF-8 string"; if (A > 1114111) throw "not a UTF-8 string"; 65535 >= A ? E += String.fromCharCode(A) : (A -= 65536, E += String.fromCharCode((A >> 10) + 55296), E += String.fromCharCode((1023 & A) + 56320)) } return E };
    this.decodeAsBytes=function(r) { var t, A, E, C, a = [], o = 0, e = r.length; "=" == r.charAt(e - 2) ? e -= 2 : "=" == r.charAt(e - 1) && (e -= 1); for (var h = 0, n = e >> 2 << 2; n > h;) t = string.base64.string.base64C[r.charAt(h++)], A = string.base64.string.base64C[r.charAt(h++)], E = string.base64.string.base64C[r.charAt(h++)], C = string.base64.string.base64C[r.charAt(h++)], a[o++] = 255 & (t << 2 | A >>> 4), a[o++] = 255 & (A << 4 | E >>> 2), a[o++] = 255 & (E << 6 | C); var D = e - n; return 2 == D ? (t = string.base64.string.base64C[r.charAt(h++)], A = string.base64.string.base64C[r.charAt(h++)], a[o++] = 255 & (t << 2 | A >>> 4)) : 3 == D && (t = string.base64.string.base64C[r.charAt(h++)], A = string.base64.string.base64C[r.charAt(h++)], E = string.base64.string.base64C[r.charAt(h++)], a[o++] = 255 & (t << 2 | A >>> 4), a[o++] = 255 & (A << 4 | E >>> 2)), a };
    this.base64C = { A: 0, B: 1, C: 2, D: 3, E: 4, F: 5, G: 6, H: 7, I: 8, J: 9, K: 10, L: 11, M: 12, N: 13, O: 14, P: 15, Q: 16, R: 17, S: 18, T: 19, U: 20, V: 21, W: 22, X: 23, Y: 24, Z: 25, a: 26, b: 27, c: 28, d: 29, e: 30, f: 31, g: 32, h: 33, i: 34, j: 35, k: 36, l: 37, m: 38, n: 39, o: 40, p: 41, q: 42, r: 43, s: 44, t: 45, u: 46, v: 47, w: 48, x: 49, y: 50, z: 51, 0: 52, 1: 53, 2: 54, 3: 55, 4: 56, 5: 57, 6: 58, 7: 59, 8: 60, 9: 61, "+": 62, "/": 63, "-": 62, _: 63 };
};

string.replaceDiacritics = function(str) {
    var diacriticsMap = {A:/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g,AA:/[\uA732]/g,AE:/[\u00C6\u01FC\u01E2\u00C4]/g,AO:/[\uA734]/g,AU:/[\uA736]/g,AV:/[\uA738\uA73A]/g,AY:/[\uA73C]/g,B:/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g,C:/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g,D:/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g,DZ:/[\u01F1\u01C4]/g,Dz:/[\u01F2\u01C5]/g,E:/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g,F:/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g,G:/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g,H:/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g,I:/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g,J:/[\u004A\u24BF\uFF2A\u0134\u0248]/g,K:/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g,L:/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g,LJ:/[\u01C7]/g,Lj:/[\u01C8]/g,M:/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g,N:/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g,NJ:/[\u01CA]/g,Nj:/[\u01CB]/g,O:/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g,OE:/[\u00D6]/g,OI:/[\u01A2]/g,OO:/[\uA74E]/g,OU:/[\u0222]/g,P:/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g,Q:/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g,R:/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g,S:/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g,T:/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g,TZ:/[\uA728]/g,U:/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g,UE:/[\u00DC]/g,V:/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g,VY:/[\uA760]/g,W:/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g,X:/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g,Y:/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g,Z:/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g,a:/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g,aa:/[\uA733]/g,ae:/[\u00E6\u01FD\u01E3\u00E4]/g,ao:/[\uA735]/g,au:/[\uA737]/g,av:/[\uA739\uA73B]/g,ay:/[\uA73D]/g,b:/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g,c:/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g,d:/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g,dz:/[\u01F3\u01C6]/g,e:/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g,f:/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g,g:/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g,h:/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g,hv:/[\u0195]/g,i:/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g,j:/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g,k:/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g,l:/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g,lj:/[\u01C9]/g,m:/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g,n:/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g,nj:/[\u01CC]/g,o:/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g,oe:/[\u00F6]/g,oi:/[\u01A3]/g,ou:/[\u0223]/g,oo:/[\uA74F]/g,p:/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g,q:/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g,r:/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g,s:/[\u0073\u24E2\uFF53\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g,ss:/[\u00DF]/g,t:/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g,tz:/[\uA729]/g,u:/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g,ue:/[\u00FC]/g,v:/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g,vy:/[\uA761]/g,w:/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g,x:/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g,y:/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g,z:/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g};
    for (var x in diacriticsMap) str = str.replace(diacriticsMap[x], x);
    return str;
};

string.chunkify = function(txt,t){
    var chunk="",
        r=new RegExp("^[\\s\\S]{"+Math.floor(50)+","+t+"}[.!?,]{1}|^[\\s\\S]{1,"+t+"}$|^[\\s\\S]{1,"+t+"} "),
        h=txt.match(r);
        return chunk=null!=h[0]&&2<=h[0].length?h[0]:chunk
};

forge.slices = {
        babla: function() {
            url = "https://en.bab.la/dictionary/{{lang}}/{{word}}";
            return;
        },
        cnrtl: function() {
            url = {
                lexicographie : "https://www.cnrtl.fr/definition/{{word}}//0",
                crisco        : "https://crisco2.unicaen.fr/des/synonymes/{{word}}",
                bob           : "https://www.languefrancaise.net/?n=Bob.Bob&action=search&q={{word}}",
                gaffiot       : "https://gaffiot.org/search/{{word}}",
                logeion       : "https://api-v2.logeion.org/detail?type=normal&w={{word}}"
            }[lang];
            return;
        },
        littre: function() {
            url = "https://www.littre.org/definition/{{word}}";
            return;
        },
        larousse: function() {
            url = `https://www.larousse.fr/dictionnaires/{{lang}}/{{word}}`;
            return;
        },
        artfl: function() {
            url = {
                dvlf            : "https://dvlf.uchicago.edu/api/mot/{{word}}",
                frantext        : "http://artflsrv02.uchicago.edu/philologic4/frantext/reports/concordance.py?report=concordance&method=proxy&q={{word}}&start=1&end=25",
                olddict         : "https://artflsrv03.uchicago.edu/philologic4/publicdicos/reports/concordance.py?report=concordance&head=&q={{word}}&start=1&end=25&direction=&metadata_sorting_field=",
                "logeion"   : "https://api-v2.logeion.org/detail?type=normal&w={{word}}",
                "perseus-el" : "http://perseus.uchicago.edu/perseus-cgi/search3torth?dbname=GreekFeb2011&word={{word}}",
                "perseus-la" : "http://perseus.uchicago.edu/perseus-cgi/search3torth?dbname=LatinAugust2012&word={{word}}",
                rogets          : "https://www.bartleby.com/cgi-bin/texis/webinator/sitesearch?FILTER=col110&query={{word}}"
            }[lang];
            word = word.toLocaleLowerCase();
            return;
        },
        wordreference: function() {
            if (lang.includes("reverse")) lang = lang.replace(" ", "/");
            url = "https://www.wordreference.com/{{lang}}/{{word}}";
            return;
        },
        etymonline: function() {
            url = lang == ""
                ? "https://www.etymonline.com/search?q={{word}}"
                : "https://quod.lib.umich.edu/m/middle-english-dictionary/dictionary?utf8=%E2%9C%93&search_field=etyma&q={{word}}";
            return;
        },
        linguee: function() {
            phrase = string.chunkify(phrase, 4900);
            url =
                lang.indexOf("deepl") > -1
                    ? ((word = phrase), "https://www.deepl.com/translator")
                    : "https://www.linguee.com/{{lang}}/search?source=auto&query={{word}}";
            return;
        },
        woxikon: function() {
            url = "https://" + (lang.indexOf("-")>-1 ? "dict":"synonyms") + ".woxikon.com/{{lang}}/{{word}}";
            return;
        },
        beolingus: function() {
            lang =
                {
                    "german-english"       : "de-en",
                    "german-english-ex"    : "de-en-ex",
                    "german-spanish"       : "de-es",
                    "german-spanish-ex"    : "de-es-ex",
                    "german-portuguese"    : "de-pt",
                    "german-portuguese-ex" : "de-pt-ex",
                    "synonyms"             : "dict-de",
                    "quote"                : "fortune-de"
                }[lang] || lang;
            url = "https://dict.tu-chemnitz.de/dings.cgi?service={{lang}}&opterrors=0&optpro=0&query={{word}}&iservice=";
            return;
        },
        thefreedictionary: function() {
            url = "https://{{lang}}.thefreedictionary.com/{{word}}";
            return;
        },
        iate: function() {
            url =
                "https://iate.europa.eu/SearchByQuery.do?method=search&query={{word}}&targetLanguages=s&sourceLanguage=en&domain=0&matching=&typeOfSearch=s&lang=en";
            return;
        },
        eurovoc: function() {
            url =
                "https://op.europa.eu/{{lang}}/web/eu-vocabularies/search-results?p_p_state=normal&p_p_lifecycle=1&p_p_id=portal2012searchExecutor_WAR_portal2012portlet_INSTANCE_3bGTrb8CGSGe&facet.collection=EUVoc&queryText={{word}}";
            return;
        },
        pons: function() {
            url = "https://en.pons.com/translate?q={{word}}&l={{lang}}";
            return;
        },
        dictcc: function() {
            url = "https://m.dict.cc/{{lang}}/?s={{word}}";
            return;
        },
        urbandictionary: function() {
            url = "https://www.urbandictionary.com/define.php?term={{word}}";
            return;
        },
        wortschatzunileipzig: function() {
            url = "https://corpora.uni-leipzig.de/de/webservice/index?panel_show=true&limit=70&corpusId={{lang}}&action=loadExamples&word={{word}}";
            return;
        },
        duden: function() {
            url = `https://www.duden.de/suchen/dudenonline/${string.replaceDiacritics(word).replace(/\-/g, "")}`;
            return;
        },
        leo: function() {
            url = "https://dict.leo.org/{{lang}}-deutsch/{{word}}";
            return;
        },
        memidex: function() {
            url = "http://www.memidex.com/{{word}}";
            return;
        },
        dictionarycom: function() {
            url = "https://www.{{lang}}.com/browse/{{word}}";
            return;
        },
        merriamwebster: function() {
        url =
            lang === "spanish"
                ? "http://www.spanishcentral.com/translate/{{word}}"
                : lang === "websters1913"
                ? "https://www.websters1913.com/words/{{word}}"
                : "https://www.merriam-webster.com/{{lang}}/{{word}}";
            return;
        },
        almaany: function() {
            url = "https://www.almaany.com/{{lang}}/dict/ar-{{lang}}/{{word}}/?c=Alles";
            return;
        },
        collins: function() {
            url = "https://www.collinsdictionary.com/dictionary/{{lang}}/{{word}}";
            return;
        },
        macmillan: function() {
            url = "https://www.macmillandictionary.com/dictionary/british/{{word}}";
            return;
        },
        oxforddictionaries: function() {
        word = string.replaceDiacritics(word);
        url =
            `https://${lang.split(" ")[0]}.oxforddictionaries.com/` +
            (["en definition", "en thesaurus", "es definition"].includes(lang)
                ? ""
                : "translate/") +
            `${lang.split(" ")[1]}/${string.replaceDiacritics(word)}`;
            return;
        },
        dwds: function() {
            url = "https://www.dwds.de/wb/" + word;
            return;
        },
        rae: function() {
            word = string.replaceDiacritics(phrase);
            url = {
                dle         : "https://{{lang}}.rae.es/?w={{word}}",
                dpd         : "http://lema.rae.es/{{lang}}/srv/search?key={{word}}",
                desen       : "http://lema.rae.es/{{lang}}/srv/search?key={{word}}",
                damer       : "http://lema.rae.es/{{lang}}/srv/search?key={{word}}",
                etimologias : "https://etimologias.dechile.net/?{{word}}"
            }[lang];
            if (url == null)
                url =
                    lang.indexOf("lar") > -1
                ? "https://www.diccionarios.com/detalle.php?palabra={{word}}&" +
                  {
                      lar1: "dicc_100=on&dicc_52=on&dicc_106=on&dicc_78=on&dicc_79=on&dicc_80=on&dicc_107=on",
                      lar2: "dicc_55=on&dicc_54=on&dicc_74=on&dicc_73=on",
                      lar3: "dicc_57=on&dicc_56=on&dicc_75=on&dicc_76=on",
                      lar4: "dicc_83=on&dicc_84=on"
                  }[lang]
                : "https://servicios.elpais.com/diccionarios/{{lang}}/{{word}}";
            return;
        },
        reverso: function(_) {
            phrase = string.chunkify(phrase,4900);
            if (_.predict && lang.length === 2) {                
                lang = {ar:"Mehdi22k",de:"Klaus22k",en:"Heather22k",es:"Maria22k",fr:"Bruno22k",it:"Chiara22k",ja:"Sakura22k",nl:"Femke22k",pt:"Celia22k",ru:"Alyona22k",zh:"Lulu22k"}[lang];
                word = phrase;             
                word = string.base64.encode(word);
                url = "http://voice2.reverso.net/RestPronunciation.svc/v1/output=json/GetVoiceStream/voiceName={{lang}}?inputText={{text}}".replace(
                    "{{lang}}",
                    lang
                );
            } else url = "https://context.reverso.net/translation/{{lang}}/{{word}}";
            return;
        },
        chambers: function() {
            lang = { 2: "21st", t: "thes", b: "biog" } [lang.substr(0, 1)];
            url = "https://chambers.co.uk/search/?query={{word}}&title={{lang}}";
            return;
        },
        cedict: function() {
            lang = lang.toLowerCase();
            url = {
                simplified  : "https://www.mdbg.net/chindict/chindict.php?page=worddict&wdrst=0&wdqtm=0&wdqcham=2&wdqt={{word}}",
                traditional : "https://www.mdbg.net/chindict/chindict.php?page=worddict&wdrst=1&wdqtm=0&wdqcham=2&wdqt={{word}}",
                hanzidico   : "http://www.hanzidico.com/php/getJsonData.php?query={{word}}&rformat=json&qtype=all",
                jukuu       : "http://www.jukuu.com/search.php?q={{word}}",
                linedict    : "https://dict.naver.com/linedict/zhendict/#/cnen/search?query={{}}",
                iciba       : "http://www.iciba.com/{{word}}",
                dvo         : "http://dict.dvo.ru/dict.php?word={{word}}&strategy=definition&dictionary=*"
            }[lang];

            word = /linedict|iciba|hanzidico/.test(lang) ? encodeURIComponent(word) : word.toLowerCase();

            return;
        },
        ispeech: function() {
            word = phrase;
            lang = { ar:"arabicmale",ca:"eurcatalanfemale",cs:"eurczechfemale",da:"eurdanishfemale",de:"eurgermanfemale",el:"eurgreekfemale",en:"auenglishfemale",en:"ukenglishfemale",es:"eurspanishfemale",fi:"eurfinnishfemale",fr:"eurfrenchfemale",hu:"huhungarianfemale",it:"euritalianfemale",ja:"jpjapanesefemale",ko:"krkoreanfemale",nl:"eurdutchfemale",no:"eurnorwegianfemale",pl:"eurpolishfemale",pt:"brportuguesefemale",pt:"eurportuguesefemale",ru:"rurussianfemale",sv:"swswedishfemale",tr:"eurturkishfemale",zh:"chchinesefemale"}[lang] || lang;
            url = "https://api.ispeech.org/api/rest?action=convert&text={{text}}&voice={{lang}}&format=mp3&apikey={{apikeyIspeach}}"
                .replace("{{lang}}", lang)
                .replace("{{apikeyIspeach}}", "ispeech-listenbutton-betauserkey");
            return;
        },
        responsivevoiceorg: function(_) {
            var gender = "female",
                sv = "g1",
                vn = "",
                res = "",
                rv=[{name:"UK English Female",tl:"en-GB"},{name:"UK English Male",tl:"en-GB"},{name:"US English Male",tl:"en-GB"},{name:"Arabic Male",tl:"ar"},{name:"Arabic Female",tl:"ar"},{name:"Armenian Male",tl:"hy"},{name:"Australian Female",tl:"en-AU"},{name:"Australian Male",tl:"en-AU"},{name:"Brazilian Portuguese Female",tl:"pt-BR"},{name:"Brazilian Portuguese Male",tl:"pt-BR"},{name:"Chinese Female",tl:"zh-CN"},{name:"Chinese Male",tl:"zh-CN"},{name:"Chinese HK Female",tl:"zh-HK"},{name:"Chinese HK Male",tl:"zh-HK"},{name:"Chinese Taiwan Female",tl:"zh-TW"},{name:"Chinese Taiwan Male",tl:"zh-TW"},{name:"Czech Female",tl:"cs"},{name:"Czech Male",tl:"cs"},{name:"Danish Female",tl:"da"},{name:"Danish Male",tl:"da"},{name:"Deutsch Female",tl:"de"},{name:"Deutsch Male",tl:"de"},{name:"Dutch Female",tl:"nl"},{name:"Dutch Male",tl:"nl"},{name:"Finnish Female",tl:"fi"},{name:"Finnish Male",tl:"fi"},{name:"French Female",tl:"fr"},{name:"French Male",tl:"fr"},{name:"Greek Female",tl:"el"},{name:"Greek Male",tl:"el"},{name:"Hindi Female",tl:"hi"},{name:"Hindi Male",tl:"hi"},{name:"Hungarian Female",tl:"hu"},{name:"Hungarian Male",tl:"hu"},{name:"Indonesian Female",tl:"id"},{name:"Indonesian Male",tl:"id"},{name:"Italian Female",tl:"it"},{name:"Italian Male",tl:"it"},{name:"Japanese Female",tl:"ja"},{name:"Japanese Male",tl:"ja"},{name:"Korean Female",tl:"ko"},{name:"Korean Male",tl:"ko"},{name:"Latin Female",tl:"la",},{name:"Latin Male",tl:"la"},{name:"Norwegian Female",tl:"nb"},{name:"Norwegian Male",tl:"nb"},{name:"Polish Female",tl:"pl"},{name:"Polish Male",tl:"pl"},{name:"Portuguese Female",tl:"pt-PT"},{name:"Portuguese Male",tl:"pt-PT"},{name:"Romanian Female",tl:"ro"},{name:"Russian Female",tl:"ru"},{name:"Russian Male",tl:"ru"},{name:"Slovak Female",tl:"sk"},{name:"Slovak Male",tl:"sk"},{name:"Spanish Male",tl:"es"},{name:"Spanish Latin American Female",tl:"es-419"},{name:"Spanish Latin American Male",tl:"es-419"},{name:"Swedish Female",tl:"sv"},{name:"Swedish Male",tl:"sv"},{name:"Thai Female",tl:"th"},{name:"Thai Male",tl:"th"},{name:"Turkish Female",tl:"tr"},{name:"Turkish Male",tl:"tr"},{name:"Moldavian Female",tl:"md"}];

            if(_.predict) {
                lang = lang == "en" ? "en-GB" : lang;
                lang = lang == "zh" ? "zh-CN" : lang;
                lang = lang == "sr-me" ? "sr" : lang;
                lang = lang == "pt" ? "pt-PT" : lang; 
                sv = !lang.match("sv|md|la") ? sv : "g2";
                vn = lang == "en-GB" && gender == "male" ? "rjs" : vn;                
                var res = rv.filter(o => o.tl == lang);
                if (!res) return;
                gender = (res.length == 1 && res[0].name.includes("Male")) ? "male" : gender;
                if(res.length > 1 ) res = res.pop();
                lang = res.tl;
            } else {
                lang = rv.filter(o=>o.name.toLowerCase()==lang)[0].tl;
            }

            word = phrase;
            url = "https://code.responsivevoice.org/getvoice.php?t={{text}}&tl={{lang}}&sv={{sv}}&vn={{vn}}&pitch=0.5&rate=0.5&vol=1&gender={{gender}}"
                .replace("{{lang}}", lang)
                .replace("{{gender}}", gender)
                .replace("{{sv}}", sv)
                .replace("{{vn}}", vn);
            return;
        },
        watson :function(){
            lang = {"birgit": "de-DE_BirgitVoice","dieter": "de-DE_DieterVoice","kate": "en-GB_KateVoice","allison": "en-US_AllisonVoice","lisa": "en-US_LisaVoice","enrique": "es-ES_EnriqueVoice","laura": "es-ES_LauraVoice","sofia_latinspanish": "es-LA_SofiaVoice","sofia_usspanish": "es-US_SofiaVoice","renee": "fr-FR_ReneeVoice","francesca": "it-IT_FrancescaVoice","emi": "ja-JP_EmiVoice","isabela": "pt-BR_IsabelaVoice","de":"de-DE_BirgitVoice","en":"en-GB_KateVoice","es":"es-LA_SofiaVoice","fr":"fr-FR_ReneeVoice","it":"it-IT_FrancescaVoice","ja":"ja-JP_EmiVoice","pt":"pt-BR_IsabelaVoice"}[lang];           
            //word = string.replaceDiacritics(phrase).replace(/%20/g,"+");
            word = phrase.replace(/(\d+)\s(\d{2,})/g,"$1$2");
            url = "https://text-to-speech-demo.ng.bluemix.net/api/synthesize?text={{text}}&voice={{lang}}&download=true&accept=audio/mp3";
        },
        triantafyllides: function() {
            lang = lang == "all" ? "" : lang + "/";
            url = "http://www.greek-language.gr/greekLang/modern_greek/tools/lexica/{{lang}}search.html?lq={{word}}&dq";
            return;
        },
        gramota: function() {
            url = "http://gramota.ru/slovari/dic/?lop=x&bts=x&zar=x&ag=x&ab=x&sin=x&lv=x&az=x&pe=x&ro=x&word={{word}}";
            return;
        },
        dsal: function() {
            url = "http://dsalsrv02.uchicago.edu/cgi-bin/philologic/search3advanced?dbname={{lang}}&query={{word}}&matchtype=exact&display=utf8";
            return;
        },
        treccani: function() {
            url =
                lang == "crusca"
                    ? "http://www.lessicografia.it/Controller?SettImpostazioni=LancioRicerca&q1={{word}}&maxresults=10&submit=Cerca+Voci&q2=&TipoSequenza=0&q3=&distanzaMax=1&Apparato1=1&Apparato2=1&Apparato3=1&Apparato4=1&EdCrusca5=1&EdCrusca1=1&EdCrusca2=1&EdCrusca3=1&EdCrusca4=1&Giunte1=1&Giunte2=1&Giunte3=1&Giunte4=1&Integrazioni=1&IgnoraAccenti=1&TipoVisualizzazione=1&TipoOrdinamento=1&TipoRicerca=0&EvidenziaKeyword=1&EvidenzSfondoContesto=1&EvidenzSfondoMicroContesto=1"
                    : `http://www.treccani.it/{{lang}}/${string.replaceDiacritics(word).toLowerCase()}`;
            if (lang == "sincon") {
                lang = "vocabolario";
                url = url + "_%28Sinonimi-e-Contrari%29";
            }
            return;
        },
        sapere: function() {
            let f = s => s.charAt(0).toUpperCase() + s.slice(1);
            if (lang.indexOf("-") > -1) {
                lang = lang
                    .split("-")
                    .map(l => f(l))
                    .join("-");
                url = `http://www.sapere.it/sapere/search.html?q=${encodeURIComponent(word)}&tipo_traduzione={{lang}}&cerca=traduzioni`;
            } else {
                word = (w => (w.slice(0, 1) + (lang == "italiano" ? "/" + w.slice(0, 2) : "")).toUpperCase() + "/" + w)(word);
                lang = f(lang);
                url = `http://www.sapere.it/sapere/dizionari/dizionari/{{lang}}/${encodeURIComponent(word)}.html`;
            }
            return;
        },
        corriere: function() {
            if (lang == "sinonimi") lang = "sincon";
            lang = {
                sabcol     : "_italiano",
                sincon     : "_sinonimi_contrari",
                init       : "_inglese/Inglese",
                itin       : "_inglese/Italiano",
                spit       : "_spagnolo/Spagnolo",
                itsp       : "_spagnolo/Italiano",
                frit       : "_francese/Francese",
                itfr       : "_francese/Italiano",
                itte       : "_tedesco/Italiano",
                teit       : "_tedesco/Tedesco",
                citazioni  : "-citazioni",
                modididire : "-modi-di-dire",
                nuomau     : ""
            }[lang];

            url = lang
                ? "https://dizionari.corriere.it/dizionario{{lang}}/" + word.substr(0, 1).toUpperCase() + `/${string.replaceDiacritics(word).toLowerCase()}.shtml`
                : `https://dizionario.internazionale.it/parola/${string.replaceDiacritics(word).toLowerCase()}`;
            return;
        },
        jmdict: function() {
            url = {
                jmdict: "https://tangorin.com/dict.php?dict=general&s={{word}}",
                weblio: "https://ejje.weblio.jp/content/{{word}}"
            }[lang];
            return;
        },
        googletranslate: function() {
            lang = lang === "zh-cn" ? "zh-CN" : lang === "zh-tw" ? "zh-TW" : lang;
            phrase = string.chunkify(phrase, 4900);
            word = phrase;
            url =
                "https://translate.google.com/?hl=en&tab=TT#view=home&op=translate&sl=auto&tl={{lang}}&text=''";
            return;
        },
        priberam: function() {
            lang = lang == "dicionário" ? "dicionario" : lang.slice(0,2).toUpperCase();
            url = "https://dicionario.priberam.org/" + (lang.length == 2 ? "Traduzir/{{lang}}/" : "") + "{{word}}";
            return;
        },
        michaelis: function() {
            url = "http://michaelis.uol.com.br/";
            lang = lang
                .replace(/ê/g, "e")
                .replace(/ã/g, "a")
                .toLowerCase();
            if (lang.indexOf("escolar") > -1 || lang.indexOf("moderno") > -1) {
                lang = lang.split("-");
                url += lang[2] + "-ingles" + "/busca/" + lang[0] + "-" + lang[1] + "/{{word}}";
                lang = lang.join("-");
            } else {
                url += "escolar-" + lang.replace("portugues", "").replace("-", "") + "/busca/" + lang + "/{{word}}";
            }
            return;
        },
        webspeechapi: function() { 
            word = phrase.replace(/(\d+)\s(\d{2,})/g,"$1$2");          
            return;
        },
        farsidics: function() {
            url = "https://www.farsidics.com/translate.php?q={{word}}";
            return;
        },
        seslisozluk: function() {
            url = "https://www.seslisozluk.net/" + (/tr|ar/.test(lang) ? `${encodeURIComponent(word)}{{lang}}/` : `{{lang}}${encodeURIComponent(word)}/`);
            lang = {
                en: "en/what-is-the-meaning-of-",
                de: "de/was-bedeutet-",
                tr: "-nedir-ne-demek",
                ru: "ru/что-значит-",
                ar: "ar/ما-هو-"
            }[lang];
            return;
        },
        moedict: function() {
            url = "https://www.moedict.tw/~{{word}}";
            return;
        },
        norstedts: function() {
            url = `https://ne.ord.se/ordbok/svenska/engelska/s%C3%B6k/${encodeURIComponent(word)}`;
            return;
        },
        spanishdict: function() {
            url = `https://www.spanishdict.com/translate/${string.replaceDiacritics(phrase)}`;
        },
        wiktionary: function() {
            //phrase = phrase.toLowerCase();
            url = `https://{{lang}}.m.wiktionary.org/wiki/${encodeURIComponent(phrase)}`;
        }       
};

forge.url = function(param) {
    url        = "",
    dictionary = param.dictionary,
    lang       = param.lang.toLowerCase(),
    word       = param.phrase,
    phrase     = word.trim().replace(/[~$%^&*_|¯<\=>\\^]/gi, "");
    word       = phrase.replace(/[\(\)\/\[\]\{\}\?\.¿؟\!¡,;:"»«“”„“‘’@#]/gi, "");

    if (!word) return {};

    this.slices[dictionary](param);  

    phrase = param.tts ? word : word.replace(/[\+]/gi, " ");
    url = url.replace(/{{word}}/gi, "{{phrase}}");
    Object.assign(param, { phrase, lang, url, dictionary });

    return param;
};

