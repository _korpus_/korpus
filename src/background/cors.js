"use strict";
/**
 * credits : https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi
 */

var accessControlRequests = {};

var exposedHeaders;

var requestRules = [
  {
    data: {
      name: "Origin",
      value: "https://www.google.de/"
    },
    mandatory: true,
    fn: null
  },
  {
    data: {
      name: "Access-Control-Request-Headers",
      value: null
    },
    mandatory: false,
    fn: function(rule, header, details) {
      if (accessControlRequests[details.requestId] === void 0) {
        accessControlRequests[details.requestId] = {};
      }
      accessControlRequests[details.requestId].headers = header.value;
    }
  }
];

var responseRules = [
  {
    data: {
      name: "Access-Control-Allow-Origin",
      value: "*"
    },
    mandatory: true,
    fn: null
  },
  {
    data: {
      name: "Access-Control-Allow-Headers",
      value: null
    },
    mandatory: true,
    fn: function(rule, header, details) {
      if (accessControlRequests[details.requestId] !== void 0) {
        header.value = accessControlRequests[details.requestId].headers;
      }
    }
  },
  {
    data: {
      name: "Access-Control-Allow-Credentials",
      value: "true"
    },
    mandatory: false,
    fn: null
  },
  {
    data: {
      name: "Access-Control-Allow-Methods",
      value: "POST, GET, OPTIONS, PUT, DELETE"
    },
    mandatory: true,
    fn: null
  },
  {
    data: {
      name: "Allow",
      value: "POST, GET, OPTIONS, PUT, DELETE"
    },
    mandatory: true,
    fn: null
  }
];

var requestListener = function(details) {
  requestRules.forEach(function(rule) {
    var flag = false;
    details.requestHeaders.forEach(function(header) {
      if (header.name === rule.data.name) {
        flag = true;
        if (rule.fn) {
          rule.fn.call(null, rule, header, details);
        } else {
          header.value = rule.data.value;
        }
      }
    });

    //add this rule anyway if it's not present in request headers
    if (!flag && rule.mandatory) {
      if (rule.data.value) {
        details.requestHeaders.push(rule.data);
      }
    }
  });

  return {
    requestHeaders: details.requestHeaders
  };
};

var responseListener = function(details) {
  responseRules.forEach(function(rule) {
    var flag = false;

    details.responseHeaders.forEach(function(header) {
      // if rule exist in response - rewrite value
      if (header.name === rule.data.name) {
        flag = true;
        if (rule.fn) {
          rule.fn.call(null, rule.data, header, details);
        } else {
          if (rule.data.value) {
            header.value = rule.data.value;
          } else {
            //@TODO DELETE this header
          }
        }
      }
    });

    //add this rule anyway if it's not present in request headers
    if (!flag && rule.mandatory) {
      if (rule.fn) {
        rule.fn.call(null, rule.data, rule.data, details);
      }

      if (rule.data.value) {
        details.responseHeaders.push(rule.data);
      }
    }
  });

  return {
    responseHeaders: details.responseHeaders
  };
};

var reload = function() {
  chrome.storage.local.get("cors", function(result) {
    result = result.cors;
    exposedHeaders = result.exposedHeaders;

    /*CORS is set to active for any http over https request
      result.active = true;
    */

    /*Remove Listeners*/
    chrome.webRequest.onHeadersReceived.removeListener(responseListener);
    chrome.webRequest.onBeforeSendHeaders.removeListener(requestListener);

    if (result.active) {
      console.log(PREFIX, "------------------CORS Enabled----------------");
      if (result.urls.length) {
        /*Add Listeners*/
        chrome.webRequest.onHeadersReceived.addListener(
          responseListener,
          {
            urls: result.urls
          },
          ["blocking", "responseHeaders"]
        );
        chrome.webRequest.onBeforeSendHeaders.addListener(
          requestListener,
          {
            urls: result.urls
          },
          ["blocking", "requestHeaders"]
        );
      }
    } else {
      if(!isMobile) chrome.browserAction.setBadgeText({ text: "" });
    }
  });
};

if(!isMobile) 
  chrome.runtime.onMessage.addListener(function(msg, snd, sndr) {
  if (msg.type == "enable__badge__cors") {
    chrome.browserAction.setBadgeText({ text: "cors" });
    chrome.browserAction.setBadgeBackgroundColor({ color: "#6a6a6d" });
  } else if (msg.type == "disable__badge__cors") {
    chrome.browserAction.setBadgeText({ text: "" });
  }
});

chrome.runtime.onInstalled.addListener(() => reload());

chrome.storage.onChanged.addListener(ps => {
  if (ps.cors) reload();
});