
"use strict";

var PREFIX = `[${chrome.runtime.getManifest().name}]`; 

var navigatr = typeof window !== "undefined" && window.navigator ? window.navigator : null,
    isMobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|playbook|bb10|blazer|compal|elaine|fennec|hiptop|iemobile|iphone|ip[oa]d|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|mobile safari|webos|mobile|tablet|\bcrmo\/|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
        navigatr && navigatr.userAgent
    );
    

var App = function(){

    var dictionaries = {
            almaany              : ["en # English", true],
            artfl                : ["dvlf # Dictionnaire vivant de la langue française", true],
            babla                : ["english-french # ", true],
            beolingus            : ["synonyms # dict-de", true],
            cedict               : ["Simplified # Chinese Pinyin English", true],
            chambers             : ["21st # 21st century dictionary", true],
            cnrtl                : ["Lexicographie # TLFi", true],
            collins              : ["english-french # ", true],
            corriere             : ["SabCol # dizionaro (Sabatini-Coletti)", true],
            dictcc               : ["ende # English German", true],
            dictionarycom        : ["Dictionary # ", true],
            dsal                 : ["Hayyim # Persian English DSAl", true],
            duden                : [" # ", true],
            dwds                 : [" # ", true],
            etymonline           : [" # EtymOnline", true],
            eurovoc              : ["en # (en) English", true],
            googletranslate      : ["fr # French FranÃ§ais", true],
            gramota              : [" # ", true],
            iate                 : ["en # English", true],
            ispeech              : ["Auto # ", true],
            watson               : ["Auto # ", true],
            jmdict               : ["JMDict # Japanese-Multilingual dictionary", true],
            larousse             : ["francais # ", true],
            leo                  : ["Französisch #", true],
            linguee              : ["DeepL # Translation", true],
            littre               : [" #  Ed. 1872-1877", true],
            memidex              : [" # ", true],
            merriamwebster       : ["Dictionary # ", true],
            michaelis            : ["portugues-brasileiro # ", true],
            oxforddictionaries   : ["EN Definition # UK English", true],
            pons                 : ["defr # German French", true],
            priberam             : ["dicionário # ", true],
            rae                  : ["DESEN # Diccionario esencial (RAE)", true],
            responsivevoiceorg   : ["Auto #", true],
            reverso              : ["english-french # ", true],
            spanishdict          : [" # ", true],
            sapere               : ["Italiano # dizionaro", true],
            thefreedictionary    : ["en # English", true],
            treccani             : ["Vocabolario # ", true],
            triantafyllides      : ["Triantafyllides # Dictionary of Standard Modern Greek", true],
            urbandictionary      : ["  # ", true],
            wordreference        : ["enfr # English French", true],
            wortschatzunileipzig : ["deu_newscrawl_2011 # German", true],
            woxikon              : ["EN-DE # English German dictionary", true],
            macmillan            : [" # ", true],
            farsidics            : [" # ", true],
            webspeechapi         : ["Auto #", true],
            seslisozluk          : ["en # English-Turkish", true],
            moedict              : [" # Chinese-Traditional", true],
            norstedts            : [" # engelska", true],
            spanishdict          : [" # ", true],
            wiktionary           : ["en # English English", true]
        },
        www = [
            "*://*.almaany.com/*",
            "*://*.api.ispeech.org/*",
            "*://*.ng.bluemix.net/*",
            "*://*.atilf.atilf.fr/*",
            "*://*.bartleby.com/*",
            "*://*.chambers.co.uk/*",
            "*://*.cnrtl.fr/*",
            "*://*.code.responsivevoice.org/*",
            "*://*.collinsdictionary.com/*",
            "*://*.context.reverso.net/*",
            "*://*.corpora.informatik.uni-leipzig.de/*",
            "*://*.crisco.unicaen.fr/*",
            "*://*.deepl.com/translator/*",
            "*://*.diccionarios.com/*",
            "*://*.dict.cc/*",
            "*://*.dict.dvo.ru/*",
            "*://*.dict.leo.org/*",
            "*://*.dict.tu-chemnitz.de/*",
            "*://*.dictionary.com/*",
            "*://*.dizionari.corriere.it/*",
            "*://*.dizionario.internazionale.it/*",
            "*://*.dsalsrv02.uchicago.edu/*",
            "*://*.duden.de/*",
            "*://*.dvlf.uchicago.edu/*",
            "*://*.dwds.de/*",
            "*://*.ejje.weblio.jp/*",
            "*://*.en.bab.la/*",
            "*://*.en.pons.com/*",
            "*://*.etimologias.dechile.net/*",
            "*://*.etymonline.com/*",
            "*://*.eurovoc.europa.eu/*",
            "*://*.farsidics.com/*",
            "*://*.gramota.ru/*",
            "*://*.greek-language.gr/*",
            "*://*.hanzidico.com/*",
            "*://*.iate.europa.eu/*",
            "*://*.jukuu.com/*",
            "*://ce.linedict.com/*",
            "*://*.iciba.com/*",
            "*://*.languefrancaise.net/*",
            "*://*.larousse.fr/*",
            "*://*.lema.rae.es/*",
            "*://*.lessicografia.it/*",
            "*://*.linguee.com/*",
            "*://*.littre.org/*",
            "*://*.macmillandictionary.com/*",
            "*://*.mdbg.net/*",
            "*://*.memidex.com/*",
            "*://*.merriam-webster.com/*",
            "*://michaelis.uol.com.br/*",
            "*://*.oxforddictionaries.com/*",
            "*://*.priberam.pt/*",
            "*://*.rae.es/*",
            "*://*.saperelb-538884594.eu-west-1.elb.amazonaws.com/*",
            "*://*.servicios.elpais.com/*",
            "*://*.spanishcentral.com/*",
            "*://*.spanishdict.com/*",
            "*://*.tangorin.com/*",
            "*://*.thefreedictionary.com/*",
            "*://*.thesaurus.com/*",
            "*://*.translate.google.com/*",
            "*://*.translate.reference.com/*",
            "*://*.treccani.it/*",
            "*://*.urbandictionary.com/*",
            "*://*.wordreference.com/*",
            "*://*.woxikon.de/*",
            "*://*.seslisozluk.net/*",
            "*://*.moedict.tw/*",
            "*://ne.ord.se/*",
            "*://wiktionary.org/*",
            "*://quod.lib.umich.edu/*"
        ];

    chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
        if (msg.type === "DOMContentLoaded" || msg.type === "resize") {
            app.sendMessage(sender.tab.id, msg);
        } else if (msg.type === "forge__url") {
            app.getLang(msg, function(data) {
                data.tts = data.method === "tts";
                app._extend(data, {
                    lang: data.lang || msg.lang,
                    tts: data.tts
                });
                msg = forge.url(data);
                app.sendMessage(sender.tab.id, msg);
            });
        }
    });

    /*this.webRequest.onHeadersReceived.addListener(
        function(details) {
            return {
                responseHeaders: details.responseHeaders.filter(function(header) {
                    return app.headers_to_strip_lowercase.indexOf(header.name.toLowerCase()) < 0;
                })
            };
        },
        {
            urls: www,
            types: ["sub_frame"]
        },
        ["blocking", "responseHeaders"]
    );*/


    /* First install / Update*/
    chrome.runtime.onInstalled.addListener(function(details) {               
        console.log(PREFIX, "previousVersion", JSON.stringify(details, null, 3));
        app.storage.clear();
        app.storage.set(
            {
                options: {
                    hotkeys: { modifier: isMobile ? "none" : "s", selection: "mouseup" },
                    extras: { height: "250", width: isMobile ? "400" : "450", speak: true, console: false, highlighter: true, inlineTranslate: false }                
                },
                cors: {
                    active: true,
                    urls: ["https://cors-anywhere.herokuapp.com/*"],
                    exposedHeaders: "",
                    Origin: "https://www.google.de/"
                },
                isMobile: isMobile,
                dictionaries: dictionaries,
                dictionary: { define: "wiktionary", tts: "responsivevoiceorg", default_translator: "googletranslate"}
            },
            ps => {
                if (chrome.runtime.lastError) console.log(PREFIX, "background storage set error", ps);
            }
        );
    });
}

App.prototype = {
    webRequest: chrome.webRequest,
    sendMessage: chrome.tabs.sendMessage,
    storage : chrome.storage.local,
    //headers_to_strip_lowercase: ["x-frame-options", "frame-options"],
    _extend: function(dst, tmp) {
        if (dst && tmp) {
            for (var key in tmp) {
                if (tmp.hasOwnProperty(key)) {
                    dst[key] = tmp[key];
                }
            }
        }
        return dst;
    },
    getLang: function(data, cb) {
        if (data.lang.indexOf("auto") > -1) {
            data.lang = data.lang.replace(/auto /, "");
            this.detectLanguage(data, cb);
        } else {
            cb(data);
        }
    },
    detectLanguage: async function(data, cb) {
        let r = await string.i18nDetectLanguage(data.phrase);
        r = !r.language ?
            (await string.richtrDetectLanguage(data.phrase), lang_guess) :
            r;
        data.lang = r ?
            ((data.lang_predicted = ISO_639_1_LANGCODE[r.language.toLowerCase()]),
                r.language + ("dictionarycom" == data.dictionary ? "#" + data.lang : "")) :
            "";
        cb(data);
    }
}

var app = new App();
