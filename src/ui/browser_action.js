(function() {
    "use strict";
    var prefs = {},
        languages = {},
        inpt = $("#tah"),
        tiles = document.getElementsByClassName("tiles")[0],
        h2 = "",
        method = "",
        selected = [],
        engine = {},
        dict = "",
        storage = chrome.storage.local,
        app,
        sortable;


    var App = function(prefs) {
        this.configList("json/languages.json", tiles);
        this.configOptions(prefs);
        this.configSlideMenu();

        document.body.addEventListener("click", function(e) {
            this.init(e);
        }.bind(this));      

        new Sortable(tiles, { /*--------------------MAKE THAT LIST SORTABLE----------------*/
            onSort: function(e) {
                let items = e.to.children,
                    r = [];
                items = Array.from(items).map(item => item.id);
                for (let index in items) r.push(languages[items[index]].rank);
                prefs.options.rank = r;
                storage.set({
                    options: prefs.options
                });
            }
        });

        chrome.storage.onChanged.addListener(ps => {
            Object.entries(ps).forEach(([key, value]) => {
                prefs[key] = value.newValue;
            });
        });
    }

    App.prototype = {
        _extend: function(dst, tmp) {
            if (dst && tmp) {
                for (var key in tmp) {
                    if (tmp.hasOwnProperty(key)) {
                        dst[key] = tmp[key];
                    }
                }
            }
            return dst;
        },
        init: function(e) {
            let target = e.target;
            if (target === tiles) return;
            if (target.classList.contains("title")) {
                dict = target.parentElement.parentElement.id;
                h2 = this.Utf8Decode(target.textContent).replace(/[#\u2716\uFE0F]/gi, "");
                this.mapDict(prefs, dict);
            } else if (target.classList.contains("c-menu--open")) {
                e.preventDefault;
                let pushRight = new Menu({
                    wrapper: "#o-wrapper",
                    type: "push-right",
                    menuOpenerClass: ".c-menu--open",
                    maskId: "#c-mask"
                });
                pushRight.open();

                document.getElementsByTagName("section")[0].onchange = function(e) {
                    target = e.target;
                    var id = target.id.replace(/--|top|left/gi, ""),
                        parent = target.parentNode.className,
                        key = id.indexOf("active") > -1 ? "cors" : id.indexOf("modifier") > -1 ? "hotkeys" : "extras";
                    if (id) {
                        key == "cors" ?
                            (prefs[parent][id] = target.checked) :
                            (prefs[parent][key][id] = target[/height|width|modifier/.test(id) ? "value" : "checked"]);
                    } else {
                        prefs.options[parent][id][target.name] = target.value;
                    }
                    storage.set({
                        options: prefs.options,
                        cors: prefs.cors
                    });
                };
            } else if (target.parentNode.classList.contains("reset")) {
                Object.keys(prefs.dictionaries).forEach(k => {
                    tiles.querySelectorAll(".tile").forEach(t => t.style.display = "block")
                    prefs.dictionaries[k][1] = true;
                });
                storage.set({ dictionaries: prefs.dictionaries });
            }
        },
        isTTS: function(a) {
            return (
                ["ispeech", "responsivevoiceorg", "watson", "webspeechapi"].indexOf(a) > -1 || (a == "reverso" && prefs.dictionaries[a][0].indexOf("Voice") > -1)
            );
        },
        configList: function(file, tiles) {
            fetch(file)
                .then(response => {
                    return response.json();
                })
                .then(json => {

                    languages = json;
                    dict = prefs.dictionary.define;

                    let IDs, ID, h2s, tile, index;
                    let sortIDs = function(json, rank) {
                            let n = [],
                                i;
                            if (rank.length < Object.keys(json).length)
                                for (i = rank.length; i < Object.keys(json).length; i++) rank.push(i + 1);
                            for (i in rank) n.push(Object.keys(json)[rank[i] - 1]);
                            return n;
                        },
                        template = function(id, content, display) {
                            var title, span, button;
                            span = document.createElement("span");
                            span.className = "title";
                            span.textContent = "#" + content;
                            button = document.createElement("button");
                            button.className = "tile-remove";
                            title = document.createElement("div");
                            title.className = "title-container";
                            title.appendChild(span);
                            tile = document.createElement("div");
                            tile.className = "tile";
                            tile.style.display = display ? "block" : "none";
                            tile.id = id;
                            tile.appendChild(title);
                            tile.appendChild(button)
                            button.addEventListener("click", (e) => { 
                                button.parentNode.style.display = "none";
                                prefs.dictionaries[id][1] = false;
                                storage.set({ dictionaries: prefs.dictionaries });
                            });
                            return tile;
                        },
                        webspeechapiVoices = async function() {
                                if (speechSynthesis) {
                                    const voices = await chooseVoice();
                                    languages.webspeechapi.lang = languages.webspeechapi.lang.concat(voices);
                                }
                            },
                            getVoices = function() {
                                return new Promise(r => {
                                    let v = speechSynthesis.getVoices();
                                    if (v.length) { r(v); return }
                                    speechSynthesis.onvoiceschanged = () => {
                                        v = speechSynthesis.getVoices();
                                        r(v)
                                    }
                                })
                            },
                            chooseVoice = async function() {
                                const aV = (await getVoices()),
                                    vs = [];
                                for (const v of aV) vs.push(v.lang + " # " + v.name);
                                return new Promise(r => { r(vs) })
                            };

                    webspeechapiVoices();

                    if (!prefs.options.rank) {
                        prefs.options.rank = Object.entries(languages).map(e => e[1].rank);
                        storage.set({
                            options: prefs.options
                        });
                    }

                    IDs = sortIDs(languages, prefs.options.rank);
                    for (ID of IDs) {
                        tile = template(ID, app.Utf8Decode(languages[ID].title), prefs.dictionaries[ID][1]);
                        tiles.appendChild(tile);
                    }

                    h2 = app.Utf8Decode(tiles.querySelector("#" + dict).textContent).replace(/[#\u2716\uFE0F]/gi, "");
                    app.headers(h2, prefs.dictionaries[dict][0]);

                    inpt = $('#tah');
                    inpt.typeahead("destroy");
                    inpt.attr("placeholder", chrome.i18n.getMessage("placeholder_lang"));
                    inpt.focus();

                    inpt.typeahead({
                            highlight: true,
                            minLength: 1
                        }, {
                            name: "languagess",
                            displayKey: "val",
                            source: function(query, syncR) {
                                engine.get(query, function(suggestions) {
                                    syncR(app.filter(suggestions));
                                });
                            },
                            templates: {
                                empty: "No matches"
                            }
                        })
                        .bind("typeahead:selected", app.select)
                    //.bind('typeahead:closed', () => { inpt.typeahead('val', '');});

                    app.mapDict(prefs, dict);
                });
            document.querySelector("#speak").parentElement.style.display = "inline-flex";
            document.body.style.width = prefs.isMobile ? "95%" : "300px";
        },
        configOptions: function(prefs) {
            let options = prefs.options,
                keysOptions = Object.keys(options),
                menu = document.getElementsByClassName("c-menu--push-right")[0],
                extras;
            for (let k of keysOptions) {
                if (k == "hotkeys") document.getElementById("modifier--" + options[k]["modifier"]).selected = true;
                if (k == "extras") {
                    extras = Object.keys(options[k]);
                    for (let a of extras) {
                        document.getElementById(a) &&
                            (document.getElementById(a)[a == "width" || a == "height" ? "value" : "checked"] = prefs.options[k][a]);
                    }
                }
            }
            document.getElementById("active").checked = prefs.cors.active;
        },
        mapDict: function(prefs, dict) {

            method = this.isTTS(dict) ? "tts" : "define";
            prefs.dictionary[method] = dict;
            storage.set({ dictionary: prefs.dictionary });
            this.headers(h2, prefs.dictionaries[dict][0]);

            selected = [];

            engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("val"),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                limit: 10,
                local: $.map(languages[dict].lang, function(l) {
                    return {
                        val: app.Utf8Decode(l)
                    };
                })
            });
            engine.initialize();

            inpt.attr("placeholder", chrome.i18n.getMessage("placeholder_lang"));
            inpt.focus();
        },
        select: function(e, datum) {
            if (datum.val) {
                if (dict == "linguee" && !/DeepL/i.test(datum.val)) {
                    selected.push(datum.val.replace(/\s#/, ""));
                    if (selected.length == 1) {
                        datum.val = selected[0] + " - ?";
                        inpt[0].style.fontWeight = 700;
                        inpt.attr("placeholder", chrome.i18n.getMessage("targetLanguage"));
                    } else {
                        datum.val = selected.join("-") + " #";
                        inpt.attr("placeholder", chrome.i18n.getMessage("placeholder_lang"));
                        inpt[0].style.fontWeight = 400;
                        selected = [];
                        inpt.val("");
                    }
                }

                if (dict == "reverso" && /Voice/.test(datum.val)) {
                    method = "tts";
                }

                app.headers(h2, datum.val);

                prefs.dictionaries[dict][0] = datum.val;
                prefs.dictionary[method] = dict;
                storage.set({
                    dictionary: prefs.dictionary,
                    dictionaries: prefs.dictionaries
                });
            }
        },
        filter: function(items) {
            return $.grep(items, function(item, index) {
                return $.inArray(item.val, []) === -1;
            });
        },
        headers: function(h2, lang) {
            inpt.val("");
            document.getElementsByClassName("dictionary")[0].textContent = h2;
            document.getElementsByClassName("lang")[0].textContent = "— " + app.Utf8Decode(lang);
        },
        Utf8Decode: function(str) {
            return (
                (str = str.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g, function(r) {
                    var e = ((15 & r.charCodeAt(0)) << 12) | ((63 & r.charCodeAt(1)) << 6) | (63 & r.charCodeAt(2));
                    return String.fromCharCode(e);
                })),
                (str = str.replace(/[\u00c0-\u00df][\u0080-\u00bf]/g, function(r) {
                    var e = ((31 & r.charCodeAt(0)) << 6) | (63 & r.charCodeAt(1));
                    return String.fromCharCode(e);
                }))
            );
        },
        configOptions: function(prefs) {
            let options = prefs.options,
                keysOptions = Object.keys(options),
                menu = document.getElementsByClassName("c-menu--push-right")[0],
                extras;
            for (let k of keysOptions) {
                if (k == "hotkeys") document.getElementById("modifier--" + options[k]["modifier"]).selected = true;
                if (k == "extras") {
                    extras = Object.keys(options[k]);
                    for (let a of extras) {
                        document.getElementById(a) &&
                            (document.getElementById(a)[a == "width" || a == "height" ? "value" : "checked"] = prefs.options[k][a]);
                    }
                }
            }
            document.getElementById("active").checked = prefs.cors.active;
        },
        configSlideMenu: function() {
            /*----SLIDE AND PUSH RIGHT MENU EFFECT------------*/
            /**credits: http://callmenick.com/post/slide-and-push-menus-with-css3-transitions **/
            (function(window) {
                function extend(a, b) {
                    for (var key in b) {
                        if (b.hasOwnProperty(key)) {
                            a[key] = b[key];
                        }
                    }
                    return a;
                }
                function each(collection, callback) {
                    for (var i = 0; i < collection.length; i++) {
                        var item = collection[i];
                        callback(item);
                    }
                }
                function Menu(options) {
                    this.options = extend({}, this.options);
                    extend(this.options, options);
                    this._init();
                }
                Menu.prototype.options = {
                    wrapper: "#o-wrapper",
                    type: "push-right",
                    menuOpenerClass: ".c-menu--open",
                    maskId: "#c-mask"
                };
                Menu.prototype._init = function() {
                    this.body = document.body;
                    this.wrapper = document.querySelector(this.options.wrapper);
                    this.mask = document.querySelector(this.options.maskId);
                    this.menu = document.querySelector("#c-menu--" + this.options.type);
                    this.closeBtn = this.menu.querySelector(".c-menu--close");
                    this.menuOpeners = document.querySelectorAll(this.options.menuOpenerClass);
                    this._initEvents();
                };
                Menu.prototype._initEvents = function() {
                    this.closeBtn.addEventListener(
                        "click",
                        function(e) {
                            e.preventDefault();
                            this.close();
                        }.bind(this)
                    );
                    this.mask.addEventListener(
                        "click",
                        function(e) {
                            e.preventDefault();
                            this.close();
                        }.bind(this)
                    );
                };
                Menu.prototype.open = function() {
                    this.body.classList.add("has-active-menu");
                    this.wrapper.classList.add("has-" + this.options.type);
                    this.menu.classList.add("is-active");
                    this.mask.classList.add("is-active");
                    this.disableMenuOpeners();
                };
                Menu.prototype.close = function() {
                    this.body.classList.remove("has-active-menu");
                    this.wrapper.classList.remove("has-" + this.options.type);
                    this.menu.classList.remove("is-active");
                    this.mask.classList.remove("is-active");
                    this.enableMenuOpeners();
                };
                Menu.prototype.disableMenuOpeners = function() {
                    each(this.menuOpeners, function(item) {
                        item.disabled = true;
                    });
                };
                Menu.prototype.enableMenuOpeners = function() {
                    each(this.menuOpeners, function(item) {
                        item.disabled = false;
                    });
                };
                window.Menu = Menu;
            })(window);
        }
    };
       
    storage.get(null, ps => {
        prefs = ps;
        app = new App(prefs);
    }); 


})();