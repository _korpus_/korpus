# KORPUS

This is a project that can be bundled as an extension, focused mainly on the firefox and chrome browsers (mobile and desktop). <br>
Korpus add one functionality of Translation, Definition or Text-to-Speech per dictionary.

<b>Main Features </b><br>
 - Curated list of dictionaries: Linguee, Wordreference, CNRTL(TLFi, CRISCO), Duden, ARTFL(DVLF, FranText, Greek-Latin), EtymOnline, Beolingus, Chambers Dictionary, Collins, DictionaryCom, Larousse, Littré, Treccani, DictCC, LEODict,  PONS, Priberam, Real Academia Española etc...

 - Translators: DeepL Translator, Google Translate <br>
    Include inline or entire page translation
 - Text-to-Speech: responsiveVoice, ISpeech, WebSpeechApi <br>
	Nota: WebSpeechApi uses the voice of your local OS, it does not need a network connection. <br>
          Make sure you have the right voice installed on your system (mobile devices already have a bunch of voices installed)<br>
    Include a current text highlighter while it is read aloud


<b>Usage</b></br>
* <em> Inline</em> Hold key "s" then select a word/phrase (key "s" + click is a default combination, you can change it).
* Via a <em>console</em> Follow this pattern " !cmd phrase " (or just " phrase or word "). Here, "!cmd" is a bang shortcut and "cmd" is either def | tts | any dictionary (at least 3/4 first characters). 

<b>Limitation</b><br>
For some dictionaries, Ublock or any other script blocker can break a search, make sure you allow your script blocker to retrieve a result from your current dictionary, translator or TTS service.

<b> Manual install </b>
 * Download this project and unzip it, 
 * Then visit about:debugging#addons (on Firefox) or chrome://extensions/ (on Chrome) and check the box for Developer mode,
 * Load it as a temporary|unpacked extension,
 * Select the extension folder or load manifest.json file.

(Nota: Korpus is already listed as an addon for firefox )
